<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:fo="http://www.w3.org/1999/XSL/Format"
                xmlns:svg="http://www.w3.org/2000/svg"
                xmlns:exsl="http://exslt.org/common"
                extension-element-prefixes="exsl"
                xmlns:rp="http://www.torstonetech.com/2012/Report"
                xmlns:o="http://www.w3.org/1999/XSL/Format">

    <xsl:include href="cs-trailer-codes_CSU.xsl"/>
    <xsl:include href="cs-report-disclaimer_CSU.xsl"/>
    <xsl:include href="cs-address-handler.xsl"/>

    <xsl:variable name="footer-height">1in</xsl:variable>
    <xsl:variable name="footer-height-first">1.2in</xsl:variable>
    <xsl:variable name="footer-height-last">1in</xsl:variable>
    <xsl:variable name="header-height">1in</xsl:variable>
    <xsl:variable name="header-height-first">3in</xsl:variable>
    <xsl:variable name="header-height-last">0in</xsl:variable>
    <xsl:variable name="region-start-size">0.625in</xsl:variable> <!-- 5/8in -->
    <xsl:variable name="region-end-size">0.5in</xsl:variable>

    <xsl:variable name="font-size-master">8pt</xsl:variable>
    <xsl:variable name="font-size-footer">9pt</xsl:variable>
    <xsl:variable name="font-size-header-bold">10pt</xsl:variable>

    <xsl:variable name="margin-top-last">0.25in</xsl:variable>
    <xsl:variable name="region-start-size-last">0.75in</xsl:variable>
    <xsl:variable name="region-end-size-last">0.75in</xsl:variable>

    <xsl:variable name="table-data-width">9.875in</xsl:variable>     <!-- 9 7/8in -->
    <xsl:variable name="table-padding-small">0.0625in</xsl:variable> <!-- 1/16in -->
    <xsl:variable name="table-padding-medium">0.125in</xsl:variable> <!-- 1/8in  -->
    <xsl:variable name="table-padding-large">0.1875in</xsl:variable> <!-- 3/16in -->
    <xsl:variable name="table-ccy-col-width">0.25in</xsl:variable>
    <xsl:variable name="table-margin-bottom">0.25in</xsl:variable>
    <xsl:variable name="table-cell-padding-right">5pt</xsl:variable>

    <xsl:variable name="col-width-date">0.75in</xsl:variable>           <!-- 3/4in -->
    <xsl:variable name="col-width-trade-date"><xsl:value-of select="$col-width-date"/></xsl:variable>       <!-- 3/4in -->
    <xsl:variable name="col-width-settlement-date"><xsl:value-of select="$col-width-date"/></xsl:variable>  <!-- 3/4in -->
    <xsl:variable name="col-width-symbol">0.75in</xsl:variable>         <!--   3/4in -->
    <xsl:variable name="col-width-description">2.5in</xsl:variable>     <!--   2.5in -->
    <xsl:variable name="col-width-transaction-type">0.5in</xsl:variable><!--   0.5in -->
    <xsl:variable name="col-width-quantity">1.125in</xsl:variable>      <!-- 1 1/8in -->
    <xsl:variable name="col-width-price">1in</xsl:variable>             <!--     1in -->
    <xsl:variable name="col-width-amount-col1">1.25in</xsl:variable>    <!-- 1 1/4in -->
    <xsl:variable name="col-width-amount-col2">1.25in</xsl:variable>    <!-- 1 1/4in -->

    <xsl:variable name="col-width-description-other-activity">3.25in</xsl:variable>


    <xsl:variable name="width-acct-value-summary">5.0625in</xsl:variable>

    <xsl:attribute-set name="page-master-csu">
        <xsl:attribute name="page-height">8.5in</xsl:attribute>
        <xsl:attribute name="page-width">11in</xsl:attribute>
        <xsl:attribute name="margin-top">0in</xsl:attribute>
        <xsl:attribute name="margin-bottom">0in</xsl:attribute>
        <xsl:attribute name="margin-left">0in</xsl:attribute>
        <xsl:attribute name="margin-right">0in</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="page-master-csu-last" use-attribute-sets="page-master-csu">
        <xsl:attribute name="margin-top">0.25in</xsl:attribute>
        <xsl:attribute name="margin-bottom">0.25in</xsl:attribute>
    </xsl:attribute-set>

    <!-- main reference areas -->
    <xsl:attribute-set name="region-body-csu">
        <xsl:attribute name="margin-top"><xsl:value-of select="$header-height"/></xsl:attribute>
        <xsl:attribute name="margin-bottom"><xsl:value-of select="$footer-height"/></xsl:attribute>
        <xsl:attribute name="margin-left"><xsl:value-of select="$region-start-size"/></xsl:attribute>
        <xsl:attribute name="margin-right"><xsl:value-of select="$region-end-size"/></xsl:attribute>
        <xsl:attribute name="display-align">before</xsl:attribute>
        <xsl:attribute name="reference-orientation">0</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="region-body-csu-first" use-attribute-sets="region-body-csu">
        <xsl:attribute name="margin-top"><xsl:value-of select="$header-height-first"/></xsl:attribute>
        <xsl:attribute name="margin-bottom"><xsl:value-of select="$footer-height-first"/></xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="region-body-csu-last" use-attribute-sets="region-body-csu">
        <xsl:attribute name="margin-top"><xsl:value-of select="$header-height-last"/></xsl:attribute>
        <xsl:attribute name="margin-bottom"><xsl:value-of select="$footer-height-last"/></xsl:attribute>
        <xsl:attribute name="margin-left"><xsl:value-of select="$region-start-size-last"/></xsl:attribute>
        <xsl:attribute name="margin-right"><xsl:value-of select="$region-end-size-last"/></xsl:attribute>
        <xsl:attribute name="display-align">center</xsl:attribute>
        <xsl:attribute name="padding">0pt</xsl:attribute>
        <xsl:attribute name="reference-orientation">0</xsl:attribute>
    </xsl:attribute-set>

    <!-- header -->
    <xsl:attribute-set name="region-before-csu">
        <xsl:attribute name="extent"><xsl:value-of select="$header-height"/></xsl:attribute>
        <xsl:attribute name="display-align">before</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="region-before-csu-first" use-attribute-sets="region-before-csu">
        <xsl:attribute name="extent"><xsl:value-of select="$header-height-first"/></xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="region-before-csu-last" use-attribute-sets="region-before-csu">
        <xsl:attribute name="extent"><xsl:value-of select="$header-height-last"/></xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="address-block">
        <xsl:attribute name="font-size">10pt</xsl:attribute>
    </xsl:attribute-set>
    <!-- footer -->
    <xsl:attribute-set name="region-after-csu">
        <xsl:attribute name="extent"><xsl:value-of select="$footer-height"/></xsl:attribute>
        <xsl:attribute name="display-align">after</xsl:attribute>
        <xsl:attribute name="font-weight">9pt</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="region-after-csu-first" use-attribute-sets="region-after-csu">
        <xsl:attribute name="extent"><xsl:value-of select="$footer-height-first"/></xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="region-after-csu-last" use-attribute-sets="region-after-csu">
        <xsl:attribute name="extent"><xsl:value-of select="$footer-height-last"/></xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="footer-image-block">
        <xsl:attribute name="block-progression-dimension.minimum">0.55in</xsl:attribute>
        <xsl:attribute name="block-progression-dimension.maximum">0.7in</xsl:attribute>
        <xsl:attribute name="block-progression-dimension.optimum">0.6in</xsl:attribute>
    </xsl:attribute-set>

    <!-- left margin -->
    <xsl:attribute-set name="region-start-csu">
        <xsl:attribute name="extent"><xsl:value-of select="$region-start-size"/></xsl:attribute>
        <xsl:attribute name="precedence">true</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="region-start-csu-last" use-attribute-sets="region-start-csu">
        <xsl:attribute name="extent"><xsl:value-of select="$region-start-size-last"/></xsl:attribute>
    </xsl:attribute-set>

    <!-- right margin -->
    <xsl:attribute-set name="region-end-csu">
        <xsl:attribute name="extent"><xsl:value-of select="$region-end-size"/></xsl:attribute>
        <xsl:attribute name="precedence">true</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="region-end-csu-last" use-attribute-sets="region-end-csu">
        <xsl:attribute name="extent"><xsl:value-of select="$region-end-size-last"/></xsl:attribute>
    </xsl:attribute-set>

    <!-- data table properties -->
    <xsl:attribute-set name="table-data">
        <xsl:attribute name="margin-bottom">0.25in</xsl:attribute>
        <xsl:attribute name="table-omit-footer-at-break">true</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="table-headline">
        <xsl:attribute name="background-color">rgb(216,216,216)</xsl:attribute>
        <xsl:attribute name="font-weight">bold</xsl:attribute>
        <xsl:attribute name="font-size">10pt</xsl:attribute>
        <xsl:attribute name="padding-top">2pt</xsl:attribute>
        <xsl:attribute name="margin-bottom">0.0625in</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="table-header-first-line" use-attribute-sets="table-headline">
        <xsl:attribute name="border-bottom">thin solid black</xsl:attribute>
        <xsl:attribute name="font-size">9pt</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="table-header-first-line-cell">
        <xsl:attribute name="padding-top">2pt</xsl:attribute>
        <xsl:attribute name="padding-bottom">2pt</xsl:attribute>
        <xsl:attribute name="padding-left"><xsl:value-of select="$table-padding-small"/></xsl:attribute>
        <xsl:attribute name="margin-bottom">0pt</xsl:attribute>
        <xsl:attribute name="display-align">center</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="table-header-second-line">
        <xsl:attribute name="font-weight">bold</xsl:attribute>
        <xsl:attribute name="border-bottom">thin solid black</xsl:attribute>
        <xsl:attribute name="font-size">7pt</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="table-summary-line">
        <xsl:attribute name="border-bottom">thin solid black</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="table-summary-line-bold">
        <xsl:attribute name="font-weight">bold</xsl:attribute>
        <xsl:attribute name="border-bottom">thick solid black</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="table-row">
        <xsl:attribute name="font-size">7pt</xsl:attribute>
        <xsl:attribute name="border-bottom">dotted black</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="table-footer-row">
        <xsl:attribute name="font-size">8pt</xsl:attribute>
        <xsl:attribute name="font-weight">bold</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="table-footer-final-row" use-attribute-sets="table-footer-row">
        <xsl:attribute name="border-bottom">thin solid black</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="account-value-summary-table">
        <xsl:attribute name="border-bottom">thin solid black</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="account-value-summary-table-bold" use-attribute-sets="account-value-summary-table">
        <xsl:attribute name="font-weight">bold</xsl:attribute>
        <xsl:attribute name="border-bottom">2pt solid black</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="transaction-summary-section-open">
        <xsl:attribute name="height">0.25in</xsl:attribute>            <!-- 1/4in -->
        <xsl:attribute name="display-align">after</xsl:attribute>
        <xsl:attribute name="padding-top">0.125in</xsl:attribute>      <!-- 1/8in -->
        <xsl:attribute name="border-top">thin solid black</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="transaction-summary-section-close">
        <xsl:attribute name="height">0.1875in</xsl:attribute>          <!-- 3/16in -->
        <xsl:attribute name="display-align">before</xsl:attribute>
        <xsl:attribute name="padding-bottom">0.0625in</xsl:attribute>  <!-- 1/16in -->
        <xsl:attribute name="border-bottom">thin solid black</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="transaction-summary-section-single">
        <xsl:attribute name="height">0.3125in</xsl:attribute>          <!-- 5/16in -->
        <xsl:attribute name="display-align">center</xsl:attribute>
        <xsl:attribute name="padding-top">0.125in</xsl:attribute>      <!-- 1/8in -->
        <xsl:attribute name="padding-bottom">0.0625in</xsl:attribute>  <!-- 1/16in -->
        <xsl:attribute name="border-top">thin solid black</xsl:attribute>
        <xsl:attribute name="border-bottom">thin solid black</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="section-title">
        <xsl:attribute name="font-weight">bold</xsl:attribute>
        <xsl:attribute name="font-size">10pt</xsl:attribute>
        <xsl:attribute name="margin-top">0.5in</xsl:attribute>
        <xsl:attribute name="margin-bottom">0.2in</xsl:attribute>
        <xsl:attribute name="keep-with-next">always</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="description-cell">
        <xsl:attribute name="padding-right"><xsl:value-of select="$table-cell-padding-right"/></xsl:attribute>
        <xsl:attribute name="keep-together.within-page">always</xsl:attribute>
    </xsl:attribute-set>

    <xsl:decimal-format NaN=" "/>

    <xsl:template match="/">
        <fo:root font-family="Credit Suisse Type Light" font-size="{$font-size-master}">
            <fo:layout-master-set>
                <fo:simple-page-master master-name="pageFirst" xsl:use-attribute-sets="page-master-csu">
                    <!-- make header taller on first page -->
                    <fo:region-body   xsl:use-attribute-sets="region-body-csu-first"/>
                    <fo:region-before xsl:use-attribute-sets="region-before-csu-first" region-name="header-first-page"/>
                    <fo:region-after  xsl:use-attribute-sets="region-after-csu-first"  region-name="footer-first-page"/>
                    <fo:region-start  xsl:use-attribute-sets="region-start-csu"/>
                    <fo:region-end    xsl:use-attribute-sets="region-end-csu"/>
                </fo:simple-page-master>

                <fo:simple-page-master master-name="page" xsl:use-attribute-sets="page-master-csu">
                    <fo:region-body   xsl:use-attribute-sets="region-body-csu"/>
                    <fo:region-before xsl:use-attribute-sets="region-before-csu" region-name="master-header"/>
                    <fo:region-after  xsl:use-attribute-sets="region-after-csu"  region-name="master-footer"/>
                    <fo:region-start  xsl:use-attribute-sets="region-start-csu"/>
                    <fo:region-end    xsl:use-attribute-sets="region-end-csu"/>
                </fo:simple-page-master>

                <fo:simple-page-master master-name="pageLast" xsl:use-attribute-sets="page-master-csu">
                    <fo:region-body   xsl:use-attribute-sets="region-body-csu-last" />
                    <fo:region-before xsl:use-attribute-sets="region-before-csu-last"/>
                    <fo:region-after  xsl:use-attribute-sets="region-after-csu-last"  region-name="footer-last"/>
                    <fo:region-start  xsl:use-attribute-sets="region-start-csu-last"/>
                    <fo:region-end    xsl:use-attribute-sets="region-end-csu-last"/>
                </fo:simple-page-master>

                <fo:page-sequence-master master-name="Layout">
                    <fo:single-page-master-reference master-reference="pageFirst"/>
                    <fo:repeatable-page-master-reference master-reference="page"/>
                </fo:page-sequence-master>

                <fo:page-sequence-master master-name="Disclaimer">
                    <fo:single-page-master-reference master-reference="pageLast"/>
                </fo:page-sequence-master>

            </fo:layout-master-set>

            <xsl:apply-templates select="statements"/>
            <xsl:apply-templates select="rp:report"/>

            <fo:page-sequence master-reference="Disclaimer">
                <!-- disclaimer on last page -->
                <fo:static-content flow-name="footer-last">
                    <xsl:call-template name="footer-csu">
                        <xsl:with-param name="displayLogo">false</xsl:with-param>
                        <xsl:with-param name="displayPageCount">false</xsl:with-param>
                    </xsl:call-template>
                </fo:static-content>

                <fo:flow flow-name="xsl-region-body">
                    <fo:block-container page-break-before="always">
                        <xsl:call-template name="cs-report-disclaimer"/>
                    </fo:block-container>
                </fo:flow>
            </fo:page-sequence> <!-- disclaimer -->
        </fo:root>
    </xsl:template>

    <xsl:template match="statements">
        <xsl:apply-templates select="rp:report"/>
    </xsl:template>

    <xsl:template match="rp:report">
        <xsl:variable name="page-count-ref" select="concat('page-count-ref-', string(position()))"/>

        <fo:page-sequence master-reference="Layout" initial-page-number="1" force-page-count="no-force" id="{$page-count-ref}">
            <fo:static-content flow-name="master-header">
                <xsl:call-template name="header-csu">
                    <xsl:with-param name="first-page-layout">false</xsl:with-param>
                    <xsl:with-param name="from-date">
                        <xsl:value-of select="rp:section[@name='StatementHeader']/rp:r/rp:c[@ref='statement_from_date']"/>
                    </xsl:with-param>
                    <xsl:with-param name="to-date">
                        <xsl:value-of select="rp:section[@name='StatementHeader']/rp:r/rp:c[@ref='statement_to_date']"/>
                    </xsl:with-param>
                    <xsl:with-param name="account-number">
                        <xsl:value-of select="rp:section[@name='StatementHeader']/rp:r/rp:c[@ref='account_number']"/>
                    </xsl:with-param>
                </xsl:call-template>
            </fo:static-content>

            <fo:static-content flow-name="header-first-page">
                <xsl:call-template name="header-csu">
                    <xsl:with-param name="first-page-layout">true</xsl:with-param>
                    <xsl:with-param name="from-date">
                        <xsl:value-of select="rp:section[@name='StatementHeader']/rp:r/rp:c[@ref='statement_from_date']"/>
                    </xsl:with-param>
                    <xsl:with-param name="to-date">
                        <xsl:value-of select="rp:section[@name='StatementHeader']/rp:r/rp:c[@ref='statement_to_date']"/>
                    </xsl:with-param>
                    <xsl:with-param name="account-number">
                        <xsl:value-of select="rp:section[@name='StatementHeader']/rp:r/rp:c[@ref='account_number']"/>
                    </xsl:with-param>
                </xsl:call-template>
            </fo:static-content>

            <fo:static-content flow-name="master-footer">
                <xsl:call-template name="footer-csu">
                    <xsl:with-param name="displayLogo">false</xsl:with-param>
                    <xsl:with-param name="pageCountRef"><xsl:value-of select="$page-count-ref"/></xsl:with-param>
                </xsl:call-template>
            </fo:static-content>

            <fo:static-content flow-name="footer-first-page">
                <xsl:call-template name="footer-csu">
                    <xsl:with-param name="displayLogo">true</xsl:with-param>
                    <xsl:with-param name="pageCountRef"><xsl:value-of select="$page-count-ref"/></xsl:with-param>
                </xsl:call-template>
            </fo:static-content>

            <fo:flow flow-name="xsl-region-body">
                <fo:block-container>
                    <fo:block-container block-progression-dimension="1.3in" display-align="after">
                        <fo:block-container inline-progression-dimension="4in">
                            <fo:block>
                                <xsl:if test="string-length(rp:section[@name='AccountSummary']/rp:r/rp:c[@ref='last_statement_date'])=0">
                                    <fo:block>Important Note:</fo:block>
                                    <fo:block>You may receive two statements for September via e-Delivery and/or mail. You are receiving both statements so you have a complete picture of your account with us for the month. Trades/Settlements at the beginning of the month may appear on both statements. You will see below that your statement does not contain a "Beginning Value". You may need to use both statements to understand your starting and ending account value. You will receive a single statement (as previous) after September 2020.</fo:block>
                                </xsl:if>
                            </fo:block>
                        </fo:block-container>
                    </fo:block-container>
                    <fo:block>
                        <fo:leader leader-pattern="rule" leader-length="100%" rule-style="solid" rule-thickness="1.5pt"/>
                    </fo:block>
                </fo:block-container>

                <fo:block-container position="fixed" top="2.2in" left="6.7in" xsl:use-attribute-sets="address-block">

                    <!-- todo: remove me tis is for testing -->
                    <xsl:choose>
                        <xsl:when test="string-length(rp:section[@name='StatementHeader']/rp:r/rp:c[@ref='address'])">
                            <xsl:call-template name="formatStatementAddress">
                                <xsl:with-param name="address">
                                    <xsl:value-of select="rp:section[@name='StatementHeader']/rp:r/rp:c[@ref='address']"/>
                                </xsl:with-param>
                            </xsl:call-template>
                        </xsl:when>
                        <xsl:otherwise>
                            <!-- TODO: this needs to go -->
                            <fo:block>NO ADDRESS</fo:block>
                        </xsl:otherwise>
                    </xsl:choose>
                </fo:block-container>

                <!-- summary tables -->
                <fo:block>
                    <fo:table>
                        <fo:table-column column-width="auto"/>
                        <fo:table-column column-width="{$width-acct-value-summary}"/>

                        <fo:table-body>
                            <fo:table-row>
                                <fo:table-cell>
                                    <xsl:apply-templates select="rp:section[@name='AccountSummary']"/>
                                </fo:table-cell>
                                <fo:table-cell>
                                    <xsl:apply-templates select="rp:section[@name='AccountValueSummary']"/>
                                </fo:table-cell>
                            </fo:table-row>
                        </fo:table-body>
                    </fo:table>
                </fo:block>

                <xsl:if test="rp:section[@name='AccountTransactionSummary']">
                    <xsl:apply-templates select="rp:section[@name='AccountTransactionSummary']"/>
                </xsl:if>

                <xsl:if test="rp:section[@name='CashAndEquivalents' or @name='Portfolio']">
                    <fo:block xsl:use-attribute-sets="section-title">
                        ACCOUNT HOLDINGS
                    </fo:block>

                    <xsl:apply-templates select="rp:section[@name='CashAndEquivalents']"/>
                    <xsl:apply-templates select="rp:section[@name='Portfolio']"/>
                </xsl:if>

                <xsl:if test="rp:section[@name='SecuritiesPurchasedOrSold' or
                                         @name='PendingTrades' or
                                         @name='DividendsAndInterestActivity' or
                                         @name='WithdrawalsAndDeposits' or
                                         @name='OtherActivity']">
                    <fo:block xsl:use-attribute-sets="section-title">
                        TRANSACTION HISTORY
                    </fo:block>

                    <xsl:apply-templates select="rp:section[@name='SecuritiesPurchasedOrSold']"/>
                    <xsl:apply-templates select="rp:section[@name='PendingTrades']"/>
                    <xsl:apply-templates select="rp:section[@name='DividendsAndInterestActivity']"/>
                    <xsl:apply-templates select="rp:section[@name='WithdrawalsAndDeposits']"/>
                    <xsl:apply-templates select="rp:section[@name='OtherActivity']"/>
                </xsl:if>
            </fo:flow>
        </fo:page-sequence> <!-- main area without the disclaimer -->
    </xsl:template>

    <xsl:template match="NUMBER">
        <xsl:variable name="numberFormat">
            <xsl:choose>
                <xsl:when test="@TYPE = 'INTEGER'">#,##0</xsl:when>
                <xsl:when test="@TYPE = 'PRICE'">#,##0.0000</xsl:when>
                <xsl:when test="@TYPE = 'SHORT_PRICE'">#,##0.00</xsl:when>
                <xsl:otherwise>#,##0.0000</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:value-of select="format-number(., $numberFormat)"/>
    </xsl:template>

    <xsl:template name="replace">
        <xsl:param name="string"/>
        <xsl:choose>
            <xsl:when test="contains($string,'&#44;')">
                <fo:block>
                    <xsl:value-of select="substring-before($string,'&#44;')"/>
                </fo:block>
                <xsl:call-template name="replace">
                    <xsl:with-param name="string" select="substring-after($string,'&#44;')"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <fo:block>
                    <xsl:value-of select="$string"/>
                </fo:block>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="header-csu">
        <xsl:param name="first-page-layout"/>
        <xsl:param name="from-date"/>
        <xsl:param name="to-date"/>
        <xsl:param name="account-number"/>

        <fo:block-container>
            <fo:block>
                <fo:table table-layout="fixed" width="100%">
                    <fo:table-column column-width="auto"/>
                    <fo:table-column column-width="auto"/>

                    <fo:table-body>
                        <fo:table-row height="{$header-height}">
                            <fo:table-cell display-align="after">
                                <fo:block block-progression-dimension="{$header-height}" display-align="after">
                                    <fo:block position="relative" top="1in">
                                        <fo:external-graphic src="url(images/credit_suisse_sp_rgb_fo_100mm.svg)" content-height="0.8in" overflow="visible"/>
                                    </fo:block> <!-- image block -->
                                </fo:block>
                            </fo:table-cell>
                            <fo:table-cell display-align="after" text-align="right">
                                <fo:block margin-top="0.25in" margin-bottom="0.17in">
                                    <xsl:if test="$first-page-layout = 'false'">
                                        <xsl:call-template name="account-statement-summary">
                                            <xsl:with-param name="from-date" select="$from-date"/>
                                            <xsl:with-param name="to-date" select="$to-date"/>
                                            <xsl:with-param name="account-number" select="$account-number"/>
                                        </xsl:call-template>
                                    </xsl:if>
                                </fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                    </fo:table-body>
                </fo:table>
            </fo:block>
            <xsl:if test="$first-page-layout = 'true'">
                <fo:block>
                    <xsl:variable name="header-left-margin">0.25in</xsl:variable>
                    <fo:block-container block-progression-dimension="1in" display-align="before" margin-left="0in">
                        <fo:block  margin-left="{$header-left-margin}">
                            <fo:block font-weight="bold" font-size="{$font-size-header-bold}">CREDIT SUISSE SECURITIES (CANADA), INC.</fo:block>
                            <fo:block>1 First Canadian Place, 29th Floor</fo:block>
                            <fo:block>Toronto, Ontario M5X 1C9</fo:block>
                        </fo:block>
                    </fo:block-container>
                    <fo:block-container block-progression-dimension="1in">
                        <fo:block font-size="{$font-size-header-bold}" margin-left="{$header-left-margin}">
                            <xsl:call-template name="account-statement-summary">
                                <xsl:with-param name="from-date" select="$from-date"/>
                                <xsl:with-param name="to-date" select="$to-date"/>
                                <xsl:with-param name="account-number" select="$account-number"/>
                            </xsl:call-template>
                        </fo:block>
                    </fo:block-container>
                </fo:block>
            </xsl:if>
        </fo:block-container>
    </xsl:template>

    <xsl:template name="account-statement-summary">
        <xsl:param name="from-date"/>
        <xsl:param name="to-date"/>
        <xsl:param name="account-number"/>

        <fo:block font-weight="bold">
            <fo:block>From <xsl:value-of select="$from-date"/> To <xsl:value-of select="$to-date"/></fo:block>
            <fo:block>Account Number: <xsl:value-of select="concat(substring($account-number,1,3),
                                                                   '-',
                                                                   substring($account-number,4))"/>
            </fo:block>
        </fo:block>
    </xsl:template>

    <xsl:template name="footer-csu">
        <xsl:param name="displayLogo">false</xsl:param>
        <xsl:param name="displayPageCount">true</xsl:param>
        <xsl:param name="pageCountRef"/>

        <fo:block block-progression-dimension="{$footer-height}"
                  block-progression-dimension.maximum="{$footer-height}"
                  font-size="{$font-size-footer}"
                  margin-bottom="0.3125in"> <!-- 5/16in -->

            <fo:table width="100%">
                <fo:table-body>
                    <fo:table-row display-align="after">
                        <fo:table-cell display-align="after">
                            <fo:block text-align="left" xsl:use-attribute-sets="footer-image-block">
                                <xsl:if test="$displayLogo = 'true'">
                                    <fo:external-graphic src="url(images/cipf.png)"
                                                         height="0.5in"
                                                         content-height="scale-down-to-fit"
                                                         padding-top="0.1in"
                                                         scaling="uniform"/>
                                </xsl:if>
                            </fo:block>
                        </fo:table-cell>

                        <fo:table-cell display-align="after">
                            <fo:block text-align="center"
                                      xsl:use-attribute-sets="footer-image-block"
                                      position="relative"
                                      bottom="-0.1in">
                                <xsl:if test="$displayLogo = 'true'">
                                    <fo:external-graphic src="url(images/RegulatedByIIROC_ENG_2col_CMYK.png)"
                                                         height="0.6in"
                                                         content-height="scale-down-to-fit"
                                                         padding-top="0.1in"
                                                         scaling="uniform"/>
                                </xsl:if>
                            </fo:block>
                        </fo:table-cell>

                        <fo:table-cell display-align="after">
                            <fo:block text-align="right">
                                <xsl:if test="$displayPageCount = 'true'">
                                    PAGE <fo:page-number/> of <fo:page-number-citation-last ref-id="{$pageCountRef}"/>
                                </xsl:if>
                            </fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                </fo:table-body>
            </fo:table>
        </fo:block>
    </xsl:template>

    <xsl:template name="summaryLineCurrencyName">
        <xsl:param name="currency_iso"/>

        <xsl:choose>
            <xsl:when test="$currency_iso = 'USD'">U.S. DOLLAR</xsl:when>
            <xsl:when test="$currency_iso = 'CAD'">CANADIAN DOLLARS</xsl:when>
            <xsl:otherwise><xsl:value-of select="$currency_iso"/></xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="summaryTableCurrencySymbol">
        <xsl:param name="currency_iso"/>

        <xsl:choose>
            <xsl:when test="$currency_iso = 'USD'">$</xsl:when>
            <xsl:when test="$currency_iso = 'CAD'">$</xsl:when>
            <xsl:otherwise><xsl:value-of select="$currency_iso"/></xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="rp:section[@name='AccountSummary']">
        <xsl:variable name="currency_name">
            <xsl:call-template name="summaryLineCurrencyName">
                <xsl:with-param name="currency_iso" select="rp:r/rp:c[@ref='currency_iso']"/>
            </xsl:call-template>
        </xsl:variable>

        <xsl:variable name="currency_short">
            <xsl:call-template name="summaryTableCurrencySymbol">
                <xsl:with-param name="currency_iso" select="rp:r/rp:c[@ref='currency_iso']"/>
            </xsl:call-template>
        </xsl:variable>

        <xsl:variable name="num-format">$#,##0.00</xsl:variable>

        <fo:block-container inline-progression-dimension="4.25in" keep-together.within-page="always">
            <fo:block use-attribute-sets="table-headline">
                ACCOUNT SUMMARY IN <xsl:value-of select="$currency_name"/>
            </fo:block>
            <xsl:if test="string-length(rp:r/rp:c[@ref='last_statement_date']) = 0">
                <!-- no particular reason for this. Last minute note adding messed up spacing so do a quick fix here -->
                <fo:block-container block-progression-dimension="0.41in">
                    <fo:block></fo:block>
                </fo:block-container>
            </xsl:if>
            <fo:table>
                <fo:table-column column-width="2.5in"/>
                <fo:table-column column-width="0.25in"/>
                <fo:table-column column-width="1.25in"/>
                <fo:table-column column-width="0.25in"/>

                <fo:table-body>
                    <xsl:choose>
                        <xsl:when test="string-length(rp:r/rp:c[@ref='last_statement_date'])">
                            <fo:table-row xsl:use-attribute-sets="table-summary-line" height="0.25in" display-align="after">
                                <fo:table-cell padding-left="{$table-padding-small}" number-columns-spanned="2">
                                    <fo:block>
                                        <fo:inline>Last Statement Date:</fo:inline>
                                        <fo:inline padding-left="0.5in">
                                            <xsl:value-of select="rp:r/rp:c[@ref='last_statement_date']"/>
                                        </fo:inline>
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell><fo:block></fo:block></fo:table-cell>
                                <fo:table-cell><fo:block></fo:block></fo:table-cell>
                            </fo:table-row>
                        </xsl:when>
                        <xsl:otherwise>
                            <!-- all of this otherwise can be removed after go-live -->
                            <fo:table-row height="0.25in" display-align="after">
                                <fo:table-cell><fo:block></fo:block></fo:table-cell>
                                <fo:table-cell><fo:block></fo:block></fo:table-cell>
                                <fo:table-cell><fo:block></fo:block></fo:table-cell>
                                <fo:table-cell><fo:block></fo:block></fo:table-cell>
                            </fo:table-row>
                        </xsl:otherwise>
                    </xsl:choose>
                    <xsl:choose>
                        <xsl:when test="string-length(rp:r/rp:c[@ref='beginning_acct_value_date'])">
                            <fo:table-row xsl:use-attribute-sets="table-summary-line" font-weight="7pt" height="0.3125in" display-align="after"> <!-- 5/16in -->
                                <fo:table-cell padding-left="{$table-padding-small}">
                                    <fo:block>
                                        Beginning Account Value
                                        <xsl:if test="string-length(rp:r/rp:c[@ref='beginning_acct_value_date'])">
                                            (On <xsl:value-of select="rp:r/rp:c[@ref='beginning_acct_value_date']"/>):
                                        </xsl:if>
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell text-align="right">
                                    <fo:block><xsl:value-of select="$currency_short"/></fo:block>
                                </fo:table-cell>
                                <fo:table-cell text-align="right">
                                    <fo:block>
                                        <xsl:value-of select="format-number(rp:r/rp:c[@ref='beginning_acct_value'], $num-format)"/>
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell><fo:block></fo:block></fo:table-cell>
                            </fo:table-row>
                        </xsl:when>
                        <xsl:otherwise>
                                <!-- this block is for first statement only -->
                            <fo:table-row>
                                <fo:table-cell><fo:block></fo:block></fo:table-cell>
                                <fo:table-cell><fo:block></fo:block></fo:table-cell>
                                <fo:table-cell><fo:block></fo:block></fo:table-cell>
                                <fo:table-cell><fo:block></fo:block></fo:table-cell>
                            </fo:table-row>
                        </xsl:otherwise>
                    </xsl:choose>

                    <xsl:choose>
                        <!-- this is the regular scenario -->
                        <xsl:when test="string-length(rp:r/rp:c[@ref='net_change'])">
                            <fo:table-row xsl:use-attribute-sets="table-summary-line" font-weight="7pt">
                                <fo:table-cell padding-left="{$table-padding-small}">
                                    <fo:block>
                                        Ending Account Value (On <xsl:value-of select="rp:r/rp:c[@ref='ending_acct_value_date']"/>):
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell text-align="right">
                                    <fo:block><xsl:value-of select="$currency_short"/></fo:block>
                                </fo:table-cell>
                                <fo:table-cell text-align="right">
                                    <fo:block>
                                        <xsl:value-of select="format-number(rp:r/rp:c[@ref='ending_acct_value'], $num-format)"/>
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell><fo:block></fo:block></fo:table-cell>
                            </fo:table-row>
                        </xsl:when>
                        <xsl:otherwise>
                            <!-- first statement scenario. Good to remove after go-live -->
                            <fo:table-row xsl:use-attribute-sets="table-summary-line-bold">
                                <fo:table-cell padding-left="{$table-padding-small}">
                                    <fo:block>
                                        Ending Account Value (On <xsl:value-of select="rp:r/rp:c[@ref='ending_acct_value_date']"/>):
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell text-align="right">
                                    <fo:block><xsl:value-of select="$currency_short"/></fo:block>
                                </fo:table-cell>
                                <fo:table-cell text-align="right">
                                    <fo:block>
                                        <xsl:value-of select="format-number(rp:r/rp:c[@ref='ending_acct_value'], $num-format)"/>
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell><fo:block></fo:block></fo:table-cell>
                            </fo:table-row>
                        </xsl:otherwise>
                    </xsl:choose>

                    <!-- net summary line doesn't appear on the first statement. Good to remove this condition after go-live -->
                    <xsl:if test="string-length(rp:r/rp:c[@ref='net_change'])">
                        <fo:table-row xsl:use-attribute-sets="table-summary-line-bold">
                            <fo:table-cell padding-left="{$table-padding-small}">
                                <fo:block>Net Change:</fo:block>
                            </fo:table-cell>
                            <fo:table-cell text-align="right">
                                <fo:block><xsl:value-of select="$currency_short"/></fo:block>
                            </fo:table-cell>
                            <fo:table-cell text-align="right">
                                <fo:block>
                                    <xsl:value-of select="format-number(rp:r/rp:c[@ref='net_change'], $num-format)"/>
                                </fo:block>
                            </fo:table-cell>
                            <fo:table-cell><fo:block></fo:block></fo:table-cell>
                        </fo:table-row>
                    </xsl:if>
                </fo:table-body>
            </fo:table>
        </fo:block-container>
    </xsl:template>

    <xsl:template match="rp:section[@name='AccountValueSummary']">
        <xsl:variable name="currency_name">
            <xsl:call-template name="summaryLineCurrencyName">
                <xsl:with-param name="currency_iso" select="rp:r/rp:c[@ref='currency_iso']"/>
            </xsl:call-template>
        </xsl:variable>

        <xsl:variable name="currency_short">
            <xsl:call-template name="summaryTableCurrencySymbol">
                <xsl:with-param name="currency_iso" select="rp:r/rp:c[@ref='currency_iso']"/>
            </xsl:call-template>
        </xsl:variable>

        <xsl:variable name="num-format">$#,##0.00</xsl:variable>

        <fo:block-container inline-progression-dimension="{$width-acct-value-summary}"  keep-together.within-page="always"> <!-- 5 1/16 in -->
            <fo:block use-attribute-sets="table-headline">
                ACCOUNT VALUE SUMMARY IN <xsl:value-of select="$currency_name"/>
            </fo:block>

            <fo:table>
                <fo:table-column column-width="2in"/>
                <fo:table-column column-width="{$table-ccy-col-width}"/>
                <fo:table-column column-width="1.25in"/>
                <fo:table-column column-width="{$table-ccy-col-width}"/>
                <fo:table-column column-width="1.25in"/>

                <fo:table-body>
                    <fo:table-row height="0.375in" font-weight="bold"> <!-- 3/8in -->
                        <fo:table-cell>
                            <fo:block></fo:block>
                        </fo:table-cell>
                        <fo:table-cell>
                            <fo:block></fo:block>
                        </fo:table-cell>
                        <fo:table-cell text-align="right">
                            <fo:block>
                                AS OF <xsl:value-of select="rp:r/rp:c[@ref='as_of_date_new']"/>
                            </fo:block>
                        </fo:table-cell>
                        <fo:table-cell>
                            <fo:block></fo:block>
                        </fo:table-cell>
                        <fo:table-cell text-align="right" padding-right="{$table-padding-medium}">
                            <fo:block>
                                <xsl:if test="string-length(rp:r/rp:c[@ref='as_of_date_old']) &gt; 0">
                                    AS OF <xsl:value-of select="rp:r/rp:c[@ref='as_of_date_old']"/>
                                </xsl:if>
                            </fo:block>
                        </fo:table-cell>
                    </fo:table-row>

                    <xsl:call-template name="account-value-summary-table-row">
                        <xsl:with-param name="title">Cash</xsl:with-param>
                        <xsl:with-param name="currency-short"><xsl:value-of select="$currency_short"/></xsl:with-param>
                        <xsl:with-param name="value-current">
                            <xsl:value-of select="rp:r/rp:c[@ref='cash_and_equivalents_new']"/>
                        </xsl:with-param>
                        <xsl:with-param name="value-old">
                            <xsl:value-of select="rp:r/rp:c[@ref='cash_and_equivalents_old']"/>
                        </xsl:with-param>
                    </xsl:call-template>

                    <xsl:call-template name="account-value-summary-table-row">
                        <xsl:with-param name="title">Margin Balance</xsl:with-param>
                        <xsl:with-param name="currency-short"><xsl:value-of select="$currency_short"/></xsl:with-param>
                        <xsl:with-param name="value-current">
                            <xsl:value-of select="rp:r/rp:c[@ref='margin_balance_new']"/>
                        </xsl:with-param>
                        <xsl:with-param name="value-old">
                            <xsl:value-of select="rp:r/rp:c[@ref='margin_balance_old']"/>
                        </xsl:with-param>
                    </xsl:call-template>

                    <xsl:call-template name="account-value-summary-table-row">
                        <xsl:with-param name="title">Total Cash/Margin Debt</xsl:with-param>
                        <xsl:with-param name="currency-short"><xsl:value-of select="$currency_short"/></xsl:with-param>
                        <xsl:with-param name="value-current">
                            <xsl:value-of select="rp:r/rp:c[@ref='total_cash_margin_new']"/>
                        </xsl:with-param>
                        <xsl:with-param name="value-old">
                            <xsl:value-of select="rp:r/rp:c[@ref='total_cash_margin_old']"/>
                        </xsl:with-param>
                    </xsl:call-template>

                    <xsl:call-template name="account-value-summary-table-row">
                        <xsl:with-param name="title">Total Value Of Securities</xsl:with-param>
                        <xsl:with-param name="currency-short"><xsl:value-of select="$currency_short"/></xsl:with-param>
                        <xsl:with-param name="value-current">
                            <xsl:value-of select="rp:r/rp:c[@ref='total_value_securities_new']"/>
                        </xsl:with-param>
                        <xsl:with-param name="value-old">
                            <xsl:value-of select="rp:r/rp:c[@ref='total_value_securities_old']"/>
                        </xsl:with-param>
                    </xsl:call-template>

                    <xsl:call-template name="account-value-summary-table-row">
                        <xsl:with-param name="title">Net Account Value</xsl:with-param>
                        <xsl:with-param name="currency-short"><xsl:value-of select="$currency_short"/></xsl:with-param>
                        <xsl:with-param name="value-current">
                            <xsl:value-of select="rp:r/rp:c[@ref='net_acct_value_new']"/>
                        </xsl:with-param>
                        <xsl:with-param name="value-old">
                            <xsl:value-of select="rp:r/rp:c[@ref='net_acct_value_old']"/>
                        </xsl:with-param>
                        <xsl:with-param name="bold">true</xsl:with-param>
                    </xsl:call-template>

                </fo:table-body>
            </fo:table>
        </fo:block-container>
    </xsl:template>

    <xsl:template match="rp:section[@name='AccountTransactionSummary']">
        <xsl:variable name="currency_name">
            <xsl:call-template name="summaryLineCurrencyName">
                <xsl:with-param name="currency_iso" select="rp:r/rp:c[@ref='currency_iso']"/>
            </xsl:call-template>
        </xsl:variable>

        <xsl:variable name="currency_short">
            <xsl:call-template name="summaryTableCurrencySymbol">
                <xsl:with-param name="currency_iso" select="rp:r/rp:c[@ref='currency_iso']"/>
            </xsl:call-template>
        </xsl:variable>

        <xsl:choose>
            <xsl:when test="count(rp:r/rp:c[node() and @ref != 'currency_iso']) = 0">
                <xsl:message>Empty Transaction Summary section. Not displaying</xsl:message>
            </xsl:when>
            <xsl:otherwise>
                <fo:block-container inline-progression-dimension="4.875in" keep-together.within-page="always">
                    <fo:block use-attribute-sets="table-headline" margin-top="1.4in" page-break-before="always">
                        ACCOUNT TRANSACTION SUMMARY IN <xsl:value-of select="$currency_name"/>
                    </fo:block>
                    <fo:table>
                        <fo:table-column column-width="1.625in"/>                   <!-- 1 5/8 in -->
                        <fo:table-column column-width="{$table-ccy-col-width}"/>    <!-- padding  -->
                        <fo:table-column column-width="1.375in"/>                   <!-- 1 3/8 in -->
                        <fo:table-column column-width="{$table-ccy-col-width}"/>    <!-- padding  -->
                        <fo:table-column column-width="1.375in"/>                   <!-- 1 3/8 in -->

                        <fo:table-body>
                            <fo:table-row height="0.375in" font-weight="bold">
                                <fo:table-cell padding-left="{$table-padding-small}">
                                    <fo:block>DESCRIPTION</fo:block>
                                </fo:table-cell>
                                <fo:table-cell>
                                    <fo:block></fo:block>
                                </fo:table-cell>
                                <fo:table-cell text-align="right">
                                    <fo:block>THIS PERIOD</fo:block>
                                </fo:table-cell>
                                <fo:table-cell>
                                    <fo:block></fo:block>
                                </fo:table-cell>
                                <fo:table-cell text-align="right" padding-right="{$table-padding-small}">
                                    <fo:block>YEAR TO DATE</fo:block>
                                </fo:table-cell>
                            </fo:table-row>

                            <!-- whether we display one or two rows in sec purchased/sold depends on if we have non-empty data
                                 we have to split is like this because of the cell borders and xsl attribute sets
                                 cannot be conditional -->
                            <xsl:choose>
                                <xsl:when test="string-length(rp:r/rp:c[@ref='period_sec_sold']) and string-length(rp:r/rp:c[@ref='period_sec_purchased'])">
                                    <xsl:call-template name="transaction-summary-table-row-open">
                                        <xsl:with-param name="row-name">Securities Purchased</xsl:with-param>
                                        <xsl:with-param name="currency-short" select="$currency_short"/>
                                        <xsl:with-param name="period-value" select="rp:r/rp:c[@ref='period_sec_purchased']"/>
                                        <xsl:with-param name="ytd-value" select="rp:r/rp:c[@ref='ytd_sec_purchased']"/>
                                    </xsl:call-template>
                                    <xsl:call-template name="transaction-summary-table-row-close">
                                        <xsl:with-param name="row-name">Securities Sold</xsl:with-param>
                                        <xsl:with-param name="currency-short" select="$currency_short"/>
                                        <xsl:with-param name="period-value" select="rp:r/rp:c[@ref='period_sec_sold']"/>
                                        <xsl:with-param name="ytd-value" select="rp:r/rp:c[@ref='ytd_sec_sold']"/>
                                    </xsl:call-template>
                                </xsl:when>
                                <xsl:when test="string-length(rp:r/rp:c[@ref='period_sec_purchased'])">
                                    <xsl:call-template name="transaction-summary-table-row-single">
                                        <xsl:with-param name="row-name">Securities Purchased</xsl:with-param>
                                        <xsl:with-param name="currency-short" select="$currency_short"/>
                                        <xsl:with-param name="period-value" select="rp:r/rp:c[@ref='period_sec_purchased']"/>
                                        <xsl:with-param name="ytd-value" select="rp:r/rp:c[@ref='ytd_sec_purchased']"/>
                                    </xsl:call-template>
                                </xsl:when>
                                <xsl:when test="string-length(rp:r/rp:c[@ref='period_sec_sold'])">
                                    <xsl:call-template name="transaction-summary-table-row-single">
                                        <xsl:with-param name="row-name">Securities Sold</xsl:with-param>
                                        <xsl:with-param name="currency-short" select="$currency_short"/>
                                        <xsl:with-param name="period-value" select="rp:r/rp:c[@ref='period_sec_sold']"/>
                                        <xsl:with-param name="ytd-value" select="rp:r/rp:c[@ref='ytd_sec_sold']"/>
                                    </xsl:call-template>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:message>Empty Security Summary data</xsl:message>
                                </xsl:otherwise>
                            </xsl:choose>

                            <xsl:choose>
                                <xsl:when test="string-length(rp:r/rp:c[@ref='period_other_activity_credited']) and string-length(rp:r/rp:c[@ref='period_other_activity_debited'])">
                                    <xsl:call-template name="transaction-summary-table-row-open">
                                        <xsl:with-param name="row-name">Other Activity Credited</xsl:with-param>
                                        <xsl:with-param name="currency-short" select="$currency_short"/>
                                        <xsl:with-param name="period-value" select="rp:r/rp:c[@ref='period_other_activity_credited']"/>
                                        <xsl:with-param name="ytd-value" select="rp:r/rp:c[@ref='ytd_other_activity_credited']"/>
                                    </xsl:call-template>
                                    <xsl:call-template name="transaction-summary-table-row-close">
                                        <xsl:with-param name="row-name">Other Activity Debited</xsl:with-param>
                                        <xsl:with-param name="currency-short" select="$currency_short"/>
                                        <xsl:with-param name="period-value" select="rp:r/rp:c[@ref='period_other_activity_debited']"/>
                                        <xsl:with-param name="ytd-value" select="rp:r/rp:c[@ref='ytd_other_activity_debited']"/>
                                    </xsl:call-template>
                                </xsl:when>
                                <xsl:when test="string-length(rp:r/rp:c[@ref='period_other_activity_credited'])">
                                    <xsl:call-template name="transaction-summary-table-row-single">
                                        <xsl:with-param name="row-name">Other Activity Credited</xsl:with-param>
                                        <xsl:with-param name="currency-short" select="$currency_short"/>
                                        <xsl:with-param name="period-value" select="rp:r/rp:c[@ref='period_other_activity_credited']"/>
                                        <xsl:with-param name="ytd-value" select="rp:r/rp:c[@ref='ytd_other_activity_credited']"/>
                                    </xsl:call-template>
                                </xsl:when>
                                <xsl:when test="string-length(rp:r/rp:c[@ref='period_other_activity_debited'])">
                                    <xsl:call-template name="transaction-summary-table-row-single">
                                        <xsl:with-param name="row-name">Other Activity Debited</xsl:with-param>
                                        <xsl:with-param name="currency-short" select="$currency_short"/>
                                        <xsl:with-param name="period-value" select="rp:r/rp:c[@ref='period_other_activity_debited']"/>
                                        <xsl:with-param name="ytd-value" select="rp:r/rp:c[@ref='ytd_other_activity_debited']"/>
                                    </xsl:call-template>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:message>Empty Other Activities summary data</xsl:message>
                                </xsl:otherwise>
                            </xsl:choose>
                        </fo:table-body>
                    </fo:table>
                    <fo:block margin-top="-0.0625in">
                        <fo:leader leader-pattern="rule" leader-length="100%" rule-style="solid" rule-thickness="1.5pt"/>
                    </fo:block>
                </fo:block-container>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="rp:section[@name='CashAndEquivalents']">
        <xsl:variable name="num-format">#,##0.00</xsl:variable>

        <fo:block>
            <fo:table xsl:use-attribute-sets="table-data">
                <fo:table-column column-width="5.625in"/>       <!-- "CASH" -->
                <fo:table-column column-width="1.625in"/>       <!-- opening balance -->
                <fo:table-column column-width="1.625in"/>       <!-- closing balance -->
                <fo:table-column column-width="1in"/>           <!-- portfolio pct -->

                <fo:table-header>
                    <fo:table-row use-attribute-sets="table-header-first-line">
                        <fo:table-cell number-columns-spanned="4" xsl:use-attribute-sets="table-header-first-line-cell">
                            <fo:block>
                                CASH (<xsl:value-of select="format-number(rp:sectionattributes/rp:sa[@name='portfolio'], $num-format)"/> % OF HOLDINGS)
                                <fo:retrieve-table-marker retrieve-class-name="continued"
                                                          retrieve-position-within-table="first-starting"
                                                          retrieve-boundary-within-table="table"/>
                            </fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row use-attribute-sets="table-header-second-line">
                        <fo:table-cell padding-left="{$table-padding-small}">
                            <fo:block>DESCRIPTION</fo:block>
                        </fo:table-cell>

                        <fo:table-cell text-align="right">
                            <fo:block>OPENING</fo:block>
                            <fo:block>BALANCE</fo:block>
                        </fo:table-cell>

                        <fo:table-cell text-align="right">
                            <fo:block>CLOSING</fo:block>
                            <fo:block>BALANCE</fo:block>
                        </fo:table-cell>

                        <fo:table-cell text-align="right">
                            <fo:block>PORTFOLIO</fo:block>
                            <fo:block>(&#37;)</fo:block>
                        </fo:table-cell>

                    </fo:table-row>
                </fo:table-header>

                <fo:table-footer>
                    <fo:table-row use-attribute-sets="table-footer-final-row">
                        <fo:table-cell padding-left="{$table-padding-small}">
                            <fo:block>TOTAL CASH</fo:block>
                        </fo:table-cell>
                        <fo:table-cell text-align="right">
                            <fo:block>
                                <xsl:call-template name="dollar">
                                    <xsl:with-param name="raw-num" select="rp:sectionattributes/rp:sa[@name='opening_balance']"/>
                                </xsl:call-template>
                            </fo:block>
                        </fo:table-cell>
                        <fo:table-cell text-align="right">
                            <fo:block>
                                <xsl:call-template name="dollar">
                                    <xsl:with-param name="raw-num" select="rp:sectionattributes/rp:sa[@name='closing_balance']"/>
                                </xsl:call-template>
                            </fo:block>
                        </fo:table-cell>
                        <fo:table-cell text-align="right">
                            <fo:block>
                                <xsl:value-of select="format-number(rp:sectionattributes/rp:sa[@name='portfolio'], $num-format)"/>&#37;
                            </fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                </fo:table-footer>

                <fo:table-body>
                    <xsl:for-each select="rp:r">
                        <fo:table-row use-attribute-sets="table-row">
                            <fo:marker marker-class-name="continued">
                                <xsl:if test="position() != 1">
                                    <fo:inline> (Continued)</fo:inline>
                                </xsl:if>
                            </fo:marker>
                            <fo:table-cell padding-left="{$table-padding-small}" xsl:use-attribute-sets="description-cell">
                                <fo:block><xsl:value-of select="rp:c[@ref='description']"/></fo:block>
                            </fo:table-cell>

                            <fo:table-cell text-align="right">
                                <fo:block><xsl:value-of select="format-number(rp:c[@ref='opening_balance'], $num-format)"/></fo:block>
                            </fo:table-cell>

                            <fo:table-cell text-align="right">
                                <fo:block><xsl:value-of select="format-number(rp:c[@ref='closing_balance'], $num-format)"/></fo:block>
                            </fo:table-cell>

                            <fo:table-cell text-align="right">
                                <fo:block><xsl:value-of select="format-number(rp:c[@ref='portfolio'], $num-format)"/></fo:block>
                            </fo:table-cell>

                        </fo:table-row>
                    </xsl:for-each>
                </fo:table-body>
            </fo:table>
        </fo:block>
    </xsl:template>

    <xsl:template match="rp:section[@name='Portfolio']">
        <xsl:variable name="num-format">#,##0.00</xsl:variable>

        <fo:block>
            <fo:table xsl:use-attribute-sets="table-data">
                <fo:table-column column-width="4in"/>           <!-- description -->
                <fo:table-column column-width="1in"/>           <!-- symbol -->
                <fo:table-column column-width="1in"/>           <!-- quantity -->
                <fo:table-column column-width="1.25in"/>        <!-- mkt value (price) -->
                <fo:table-column column-width="1.625in"/>       <!-- total mkt value -->
                <fo:table-column column-width="1in"/>           <!-- portfolio pct -->

                <fo:table-header>
                    <fo:table-row use-attribute-sets="table-header-first-line">
                        <fo:table-cell number-columns-spanned="6" xsl:use-attribute-sets="table-header-first-line-cell">
                            <fo:block>
                                PORTFOLIO (<xsl:value-of select="format-number(rp:sectionattributes/rp:sa[@name='portfolio'], $num-format)"/> % OF HOLDINGS)
                                <fo:retrieve-table-marker retrieve-class-name="continued"
                                                          retrieve-position-within-table="first-starting"
                                                          retrieve-boundary-within-table="table"/>
                            </fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row use-attribute-sets="table-header-second-line">
                        <fo:table-cell padding-left="{$table-padding-small}">
                            <fo:block>DESCRIPTION</fo:block>
                        </fo:table-cell>

                        <fo:table-cell>
                            <fo:block>SYMBOL&#47;</fo:block>
                            <fo:block>CUSIP</fo:block>
                        </fo:table-cell>

                        <fo:table-cell text-align="right">
                            <fo:block>QUANTITY</fo:block>
                        </fo:table-cell>

                        <fo:table-cell text-align="right">
                            <fo:block>PRICE</fo:block>
                        </fo:table-cell>

                        <fo:table-cell text-align="right">
                            <fo:block>MARKET VALUE</fo:block>
                        </fo:table-cell>

                        <fo:table-cell text-align="right">
                            <fo:block>PORTFOLIO</fo:block>
                            <fo:block>(&#37;)</fo:block>
                        </fo:table-cell>

                    </fo:table-row>
                </fo:table-header>

                <fo:table-footer>
                    <fo:table-row use-attribute-sets="table-footer-final-row">
                        <fo:table-cell number-columns-spanned="4" padding-left="{$table-padding-small}">
                            <fo:block>TOTAL PORTFOLIO</fo:block>
                        </fo:table-cell>
                        <fo:table-cell text-align="right">
                            <fo:block>
                                <xsl:call-template name="dollar">
                                    <xsl:with-param name="raw-num" select="rp:sectionattributes/rp:sa[@name='total_mkt_value']"/>
                                </xsl:call-template>
                            </fo:block>
                        </fo:table-cell>
                        <fo:table-cell text-align="right">
                            <fo:block>
                                <xsl:value-of select="format-number(rp:sectionattributes/rp:sa[@name='portfolio'], $num-format)"/>&#37;
                            </fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                </fo:table-footer>

                <fo:table-body>
                    <xsl:for-each select="rp:r">
                        <fo:table-row use-attribute-sets="table-row">
                            <fo:marker marker-class-name="continued">
                                <xsl:if test="position() != 1">
                                    <fo:inline> (Continued)</fo:inline>
                                </xsl:if>
                            </fo:marker>
                            <fo:table-cell padding-left="{$table-padding-small}" xsl:use-attribute-sets="description-cell">
                                <fo:block><xsl:apply-templates select="rp:c[@ref='description']"/></fo:block>
                            </fo:table-cell>

                            <fo:table-cell>
                                <fo:block><xsl:value-of select="rp:c[@ref='symbol']"/></fo:block>
                            </fo:table-cell>

                            <fo:table-cell text-align="right">
                                <fo:block><xsl:value-of select="format-number(rp:c[@ref='quantity'], '#,##0)')"/></fo:block>
                            </fo:table-cell>

                            <fo:table-cell text-align="right">
                                <fo:block><xsl:value-of select="format-number(rp:c[@ref='market_value'], '#,##0.0000')"/></fo:block>
                            </fo:table-cell>

                            <fo:table-cell text-align="right">
                                <fo:block><xsl:value-of select="format-number(rp:c[@ref='total_mkt_value'], $num-format)"/></fo:block>
                            </fo:table-cell>

                            <fo:table-cell text-align="right">
            <fo:block><xsl:value-of select="format-number(rp:c[@ref='portfolio'], $num-format)"/></fo:block>
                            </fo:table-cell>

                        </fo:table-row>
                    </xsl:for-each>
                </fo:table-body>
            </fo:table>
        </fo:block>
    </xsl:template>

    <xsl:template match="rp:section[@name='SecuritiesPurchasedOrSold']">
        <xsl:choose>
            <xsl:when test="count(rp:r) = 0">
                <fo:block>No securites purchased or sold to display</fo:block>
            </xsl:when>
            <xsl:otherwise>
                <fo:block>
                    <fo:table xsl:use-attribute-sets="table-data">
                        <fo:table-column column-width="{$col-width-trade-date}"/>
                        <fo:table-column column-width="{$col-width-settlement-date}"/>
                        <fo:table-column column-width="{$col-width-symbol}"/>
                        <fo:table-column column-width="{$col-width-description}"/>
                        <fo:table-column column-width="{$col-width-transaction-type}"/>
                        <fo:table-column column-width="{$col-width-quantity}"/>
                        <fo:table-column column-width="{$col-width-price}"/>
                        <fo:table-column column-width="{$col-width-amount-col1}"/>
                        <fo:table-column column-width="{$col-width-amount-col2}"/>

                        <fo:table-header>
                            <fo:table-row use-attribute-sets="table-header-first-line">
                                <fo:table-cell number-columns-spanned="9" xsl:use-attribute-sets="table-header-first-line-cell">
                                    <fo:block>
                                        SECURITIES PURCHASED OR SOLD
                                        <fo:retrieve-table-marker retrieve-class-name="continued"
                                                                  retrieve-position-within-table="first-starting"
                                                                  retrieve-boundary-within-table="table"/>
                                    </fo:block>
                                </fo:table-cell>
                            </fo:table-row>
                            <fo:table-row use-attribute-sets="table-header-second-line">
                                <fo:table-cell padding-left="{$table-padding-small}">
                                    <fo:block>TRADE</fo:block>
                                    <fo:block>DATE</fo:block>
                                </fo:table-cell>

                                <fo:table-cell>
                                    <fo:block>SETTLEMENT</fo:block>
                                    <fo:block>DATE</fo:block>
                                </fo:table-cell>

                                <fo:table-cell>
                                    <fo:block>SYMBOL&#47;</fo:block>
                                    <fo:block>CUSIP</fo:block>
                                </fo:table-cell>

                                <fo:table-cell>
                                    <fo:block>DESCRIPTION</fo:block>
                                </fo:table-cell>

                                <fo:table-cell>
                                    <fo:block>TRANSACTION</fo:block>
                                    <fo:block>TYPE</fo:block>
                                </fo:table-cell>

                                <fo:table-cell text-align="right">
                                    <fo:block>QUANTITY</fo:block>
                                </fo:table-cell>

                                <fo:table-cell text-align="right">
                                    <fo:block>PRICE</fo:block>
                                </fo:table-cell>

                                <fo:table-cell text-align="right">
                                    <fo:block>AMOUNT</fo:block>
                                    <fo:block>PURCHASED</fo:block>
                                </fo:table-cell>

                                <fo:table-cell text-align="right" padding-right="{$table-padding-small}">
                                    <fo:block>AMOUNT</fo:block>
                                    <fo:block>SOLD</fo:block>
                                </fo:table-cell>
                            </fo:table-row>
                        </fo:table-header>

                        <fo:table-footer>
                            <fo:table-row use-attribute-sets="table-footer-final-row">
                                <fo:table-cell number-columns-spanned="7" padding-left="{$table-padding-small}">
                                    <fo:block>TOTAL SECURITIES ACTIVITY</fo:block>
                                </fo:table-cell>
                                <fo:table-cell text-align="right">
                                    <fo:block>
                                        <xsl:call-template name="dollar">
                                            <xsl:with-param name="raw-num" select="rp:sectionattributes/rp:sa[@name='amount_purchased']"/>
                                        </xsl:call-template>
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell text-align="right" padding-right="{$table-padding-small}">
                                    <fo:block>
                                        <xsl:call-template name="dollar">
                                            <xsl:with-param name="raw-num" select="rp:sectionattributes/rp:sa[@name='amount_sold']"/>
                                        </xsl:call-template>
                                    </fo:block>
                                </fo:table-cell>
                            </fo:table-row>
                        </fo:table-footer>

                        <fo:table-body>
                            <xsl:for-each select="rp:r">
                                <fo:table-row use-attribute-sets="table-row">
                                    <fo:marker marker-class-name="continued">
                                        <xsl:if test="position() != 1">
                                            <fo:inline> (Continued)</fo:inline>
                                        </xsl:if>
                                    </fo:marker>
                                    <fo:table-cell padding-left="{$table-padding-small}">
                                        <fo:block><xsl:value-of select="rp:c[@ref='trade_date']"/></fo:block>
                                    </fo:table-cell>

                                    <fo:table-cell>
                                        <fo:block><xsl:value-of select="rp:c[@ref='settlement_date']"/></fo:block>
                                    </fo:table-cell>

                                    <fo:table-cell>
                                        <fo:block><xsl:value-of select="rp:c[@ref='symbol']"/></fo:block>
                                    </fo:table-cell>

                                    <fo:table-cell xsl:use-attribute-sets="description-cell">
                                        <fo:block>
                                            <fo:block><xsl:value-of select="rp:c[@ref='description']"/></fo:block>
                                            <xsl:call-template name="separatedStringToBlocks">
                                                <xsl:with-param name="data" select="rp:c[@ref='memos_delim']"/>
                                            </xsl:call-template>
                                            <xsl:call-template name="separatedStringToBlocks">
                                                <xsl:with-param name="data" select="rp:c[@ref='trailers_delim']"/>
                                                <xsl:with-param name="isTrailerCode">true</xsl:with-param>
                                            </xsl:call-template>
                                        </fo:block>
                                    </fo:table-cell>

                                    <fo:table-cell>
                                        <fo:block><xsl:value-of select="rp:c[@ref='transaction_type']"/></fo:block>
                                    </fo:table-cell>

                                    <fo:table-cell text-align="right">
                                        <fo:block><xsl:value-of select="format-number(rp:c[@ref='quantity'], '#,##0')"/></fo:block>
                                    </fo:table-cell>

                                    <fo:table-cell text-align="right">
                                        <fo:block><xsl:value-of select="format-number(rp:c[@ref='price'], '#,##0.0000')"/></fo:block>
                                    </fo:table-cell>

                                    <fo:table-cell text-align="right">
                                        <fo:block><xsl:value-of select="format-number(rp:c[@ref='amount_purchased'], '#,##0.00')"/></fo:block>
                                    </fo:table-cell>

                                    <fo:table-cell text-align="right" padding-right="{$table-padding-small}">
                                        <fo:block><xsl:value-of select="format-number(rp:c[@ref='amount_sold'], '#,##0.00')"/></fo:block>
                                    </fo:table-cell>
                                </fo:table-row>
                            </xsl:for-each>
                        </fo:table-body>
                    </fo:table>
                </fo:block>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="rp:section[@name='PendingTrades']">
        <xsl:choose>
            <xsl:when test="count(rp:r) = 0">
                <fo:block>No pending trades to display</fo:block>
            </xsl:when>
            <xsl:otherwise>
                <xsl:variable name="num-format">$#,##0.00</xsl:variable>

                <fo:block>
                    <fo:table xsl:use-attribute-sets="table-data">
                        <fo:table-column column-width="{$col-width-trade-date}"/>
                        <fo:table-column column-width="{$col-width-settlement-date}"/>
                        <fo:table-column column-width="{$col-width-symbol}"/>
                        <fo:table-column column-width="{$col-width-description}"/>
                        <fo:table-column column-width="{$col-width-transaction-type}"/>
                        <fo:table-column column-width="{$col-width-quantity}"/>
                        <fo:table-column column-width="{$col-width-price}"/>
                        <fo:table-column column-width="{$col-width-amount-col1}"/>
                        <fo:table-column column-width="{$col-width-amount-col2}"/>

                        <fo:table-header>
                            <fo:table-row use-attribute-sets="table-header-first-line">
                                <fo:table-cell number-columns-spanned="9" xsl:use-attribute-sets="table-header-first-line-cell">
                                    <fo:block>
                                        PENDING TRADES
                                        <fo:retrieve-table-marker retrieve-class-name="continued"
                                                                  retrieve-position-within-table="first-starting"
                                                                  retrieve-boundary-within-table="table"/>
                                    </fo:block>
                                </fo:table-cell>
                            </fo:table-row>
                            <fo:table-row use-attribute-sets="table-header-second-line">
                                <fo:table-cell padding-left="{$table-padding-small}">
                                    <fo:block>TRADE</fo:block>
                                    <fo:block>DATE</fo:block>
                                </fo:table-cell>

                                <fo:table-cell>
                                    <fo:block>SETTLEMENT</fo:block>
                                    <fo:block>DATE</fo:block>
                                </fo:table-cell>

                                <fo:table-cell>
                                    <fo:block>SYMBOL&#47;</fo:block>
                                    <fo:block>CUSIP</fo:block>
                                </fo:table-cell>

                                <fo:table-cell>
                                    <fo:block>DESCRIPTION</fo:block>
                                </fo:table-cell>

                                <fo:table-cell>
                                    <fo:block>TRANSACTION</fo:block>
                                    <fo:block>TYPE</fo:block>
                                </fo:table-cell>

                                <fo:table-cell text-align="right">
                                    <fo:block>QUANTITY</fo:block>
                                </fo:table-cell>

                                <fo:table-cell text-align="right">
                                    <fo:block>PRICE</fo:block>
                                </fo:table-cell>

                                <fo:table-cell text-align="right">
                                    <fo:block>AMOUNT</fo:block>
                                    <fo:block>PURCHASED</fo:block>
                                </fo:table-cell>

                                <fo:table-cell text-align="right" padding-right="{$table-padding-small}">
                                    <fo:block>AMOUNT</fo:block>
                                    <fo:block>SOLD</fo:block>
                                </fo:table-cell>
                            </fo:table-row>
                        </fo:table-header>

                        <fo:table-footer>
                            <fo:table-row use-attribute-sets="table-footer-final-row">
                                <fo:table-cell number-columns-spanned="7" padding-left="{$table-padding-small}">
                                    <fo:block>TOTAL PENDING TRADES</fo:block>
                                </fo:table-cell>
                                <fo:table-cell text-align="right">
                                    <fo:block>
                                        <xsl:call-template name="dollar">
                                            <xsl:with-param name="raw-num" select="rp:sectionattributes/rp:sa[@name='amount_purchased']"/>
                                        </xsl:call-template>
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell text-align="right" padding-right="{$table-padding-small}">
                                    <fo:block>
                                        <xsl:call-template name="dollar">
                                            <xsl:with-param name="raw-num" select="rp:sectionattributes/rp:sa[@name='amount_sold']"/>
                                        </xsl:call-template>
                                    </fo:block>
                                </fo:table-cell>
                            </fo:table-row>
                        </fo:table-footer>

                        <fo:table-body>
                            <xsl:for-each select="rp:r">
                                <fo:table-row use-attribute-sets="table-row">
                                    <fo:marker marker-class-name="continued">
                                        <xsl:if test="position() != 1">
                                            <fo:inline> (Continued)</fo:inline>
                                        </xsl:if>
                                    </fo:marker>
                                    <fo:table-cell padding-left="{$table-padding-small}">
                                        <fo:block><xsl:value-of select="rp:c[@ref='trade_date']"/></fo:block>
                                    </fo:table-cell>

                                    <fo:table-cell>
                                        <fo:block><xsl:value-of select="rp:c[@ref='settlement_date']"/></fo:block>
                                    </fo:table-cell>

                                    <fo:table-cell>
                                        <fo:block><xsl:value-of select="rp:c[@ref='symbol']"/></fo:block>
                                    </fo:table-cell>

                                    <fo:table-cell xsl:use-attribute-sets="description-cell">
                                        <fo:block>
                                            <fo:block><xsl:value-of select="rp:c[@ref='description']"/></fo:block>
                                            <xsl:call-template name="separatedStringToBlocks">
                                                <xsl:with-param name="data" select="rp:c[@ref='memos_delim']"/>
                                            </xsl:call-template>
                                            <xsl:call-template name="separatedStringToBlocks">
                                                <xsl:with-param name="data" select="rp:c[@ref='trailers_delim']"/>
                                                <xsl:with-param name="isTrailerCode">true</xsl:with-param>
                                            </xsl:call-template>
                                        </fo:block>
                                    </fo:table-cell>

                                    <fo:table-cell>
                                        <fo:block><xsl:value-of select="rp:c[@ref='transaction_type']"/></fo:block>
                                    </fo:table-cell>

                                    <fo:table-cell text-align="right">
                                        <fo:block><xsl:value-of select="format-number(rp:c[@ref='quantity'], '#,##0')"/></fo:block>
                                    </fo:table-cell>

                                    <fo:table-cell text-align="right">
                                        <fo:block><xsl:value-of select="format-number(rp:c[@ref='price'], '#,##0.0000')"/></fo:block>
                                    </fo:table-cell>

                                    <fo:table-cell text-align="right">
                                        <fo:block><xsl:value-of select="format-number(rp:c[@ref='amount_purchased'], $num-format)"/></fo:block>
                                    </fo:table-cell>

                                    <fo:table-cell text-align="right" padding-right="{$table-padding-small}">
                                        <fo:block><xsl:value-of select="format-number(rp:c[@ref='amount_sold'], $num-format)"/></fo:block>
                                    </fo:table-cell>
                                </fo:table-row>
                            </xsl:for-each>
                        </fo:table-body>
                    </fo:table>
                </fo:block>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="rp:section[@name='DividendsAndInterestActivity']">
        <fo:block>
            <fo:table xsl:use-attribute-sets="table-data">
                <fo:table-column column-width="0.625in"/>   <!-- 5/8in -->
                <fo:table-column column-width="0.875in"/>   <!-- 7/8in -->
                <fo:table-column column-width="1.375in"/>   <!-- 1 3/8in -->
                <fo:table-column column-width="4in"/>
                <fo:table-column column-width="1.5in"/>
                <fo:table-column column-width="1.5in"/>

                <fo:table-header>
                    <fo:table-row use-attribute-sets="table-header-first-line">
                        <fo:table-cell number-columns-spanned="6" xsl:use-attribute-sets="table-header-first-line-cell">
                            <fo:block>
                                DIVIDENDS &amp; INTEREST ACTIVITY
                                <fo:retrieve-table-marker retrieve-class-name="continued"
                                                          retrieve-position-within-table="first-starting"
                                                          retrieve-boundary-within-table="table"/>
                            </fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row use-attribute-sets="table-header-second-line">
                        <fo:table-cell padding-left="{$table-padding-small}">
                            <fo:block>DATE</fo:block>
                        </fo:table-cell>

                        <fo:table-cell>
                            <fo:block>TRANSACTION</fo:block>
                            <fo:block>TYPE</fo:block>
                        </fo:table-cell>

                        <fo:table-cell>
                            <fo:block>SYMBOL&#47;</fo:block>
                            <fo:block>CUSIP</fo:block>
                        </fo:table-cell>

                        <fo:table-cell>
                            <fo:block>DESCRIPTION</fo:block>
                        </fo:table-cell>

                        <fo:table-cell text-align="right">
                            <fo:block>AMOUNT</fo:block>
                            <fo:block>DEBITED</fo:block>
                        </fo:table-cell>

                        <fo:table-cell text-align="right" padding-right="{$table-padding-small}">
                            <fo:block>AMOUNT</fo:block>
                            <fo:block>CREDITED</fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                </fo:table-header>

                <fo:table-footer>
                    <fo:table-row use-attribute-sets="table-footer-row">
                        <fo:table-cell number-columns-spanned="4" padding-left="{$table-padding-small}">
                            <fo:block>TOTAL DIVIDENDS &amp; INTEREST ACTIVITY</fo:block>
                        </fo:table-cell>
                        <fo:table-cell text-align="right">
                            <fo:block>
                                <xsl:value-of select="rp:sectionattributes/rp:sa[@name='total_amount_debited']"/>
                            </fo:block>
                        </fo:table-cell>
                        <fo:table-cell text-align="right" padding-right="{$table-padding-small}">
                            <fo:block>
                                <xsl:value-of select="rp:sectionattributes/rp:sa[@name='total_amount_credited']"/>
                            </fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row use-attribute-sets="table-footer-final-row">
                        <fo:table-cell number-columns-spanned="5" padding-left="{$table-padding-small}">
                            <fo:block>NET DIVIDENDS &amp; INTEREST ACTIVITY</fo:block>
                        </fo:table-cell>
                        <fo:table-cell text-align="right" padding-right="{$table-padding-small}">
                            <fo:block>
                                <xsl:value-of select="rp:sectionattributes/rp:sa[@name='net_activity']"/>
                            </fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                </fo:table-footer>

                <fo:table-body>
                    <xsl:for-each select="rp:r">
                        <fo:table-row use-attribute-sets="table-row">
                            <fo:marker marker-class-name="continued">
                                <xsl:if test="position() != 1">
                                    <fo:inline> (Continued)</fo:inline>
                                </xsl:if>
                            </fo:marker>
                            <fo:table-cell padding-left="{$table-padding-small}">
                                <fo:block><xsl:value-of select="rp:c[@ref='date']"/></fo:block>
                            </fo:table-cell>

                            <fo:table-cell>
                                <fo:block><xsl:value-of select="rp:c[@ref='transaction_type']"/></fo:block>
                            </fo:table-cell>

                            <fo:table-cell>
                                <fo:block><xsl:value-of select="rp:c[@ref='symbol']"/></fo:block>
                            </fo:table-cell>

                            <fo:table-cell xsl:use-attribute-sets="description-cell">
                                <fo:block>
                                    <fo:block><xsl:value-of select="rp:c[@ref='description']"/></fo:block>
                                    <xsl:call-template name="separatedStringToBlocks">
                                        <xsl:with-param name="data" select="rp:c[@ref='memos_delim']"/>
                                    </xsl:call-template>
                                    <xsl:call-template name="separatedStringToBlocks">
                                        <xsl:with-param name="data" select="rp:c[@ref='trailers_delim']"/>
                                        <xsl:with-param name="isTrailerCode">true</xsl:with-param>
                                    </xsl:call-template>
                                </fo:block>
                            </fo:table-cell>

                            <fo:table-cell text-align="right">
                                <fo:block><xsl:value-of select="rp:c[@ref='amount_debited']"/></fo:block>
                            </fo:table-cell>

                            <fo:table-cell text-align="right" padding-right="{$table-padding-small}">
                                <fo:block><xsl:value-of select="rp:c[@ref='amount_credited']"/></fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                    </xsl:for-each>
                </fo:table-body>
            </fo:table>
        </fo:block>
    </xsl:template>

    <xsl:template match="rp:section[@name='WithdrawalsAndDeposits']">
        <fo:block>
            <fo:table xsl:use-attribute-sets="table-data">
                <fo:table-column column-width="0.625in"/>   <!--   5/8in -->
                <fo:table-column column-width="1in"/>
                <fo:table-column column-width="5.25in"/>
                <fo:table-column column-width="1.5in"/>
                <fo:table-column column-width="1.5in"/>

                <fo:table-header>
                    <fo:table-row use-attribute-sets="table-header-first-line">
                        <fo:table-cell number-columns-spanned="5" xsl:use-attribute-sets="table-header-first-line-cell">
                            <fo:block>
                                WITHDRAWALS &amp; DEPOSITS
                                <fo:retrieve-table-marker retrieve-class-name="continued"
                                                          retrieve-position-within-table="first-starting"
                                                          retrieve-boundary-within-table="table"/>
                            </fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row use-attribute-sets="table-header-second-line">
                        <fo:table-cell padding-left="{$table-padding-small}">
                            <fo:block>DATE</fo:block>
                        </fo:table-cell>

                        <fo:table-cell>
                            <fo:block>TRANSACTION</fo:block>
                            <fo:block>TYPE</fo:block>
                        </fo:table-cell>

                        <fo:table-cell>
                            <fo:block>DESCRIPTION</fo:block>
                        </fo:table-cell>

                        <fo:table-cell text-align="right">
                            <fo:block>WITHDRAWALS</fo:block>
                        </fo:table-cell>

                        <fo:table-cell text-align="right" padding-right="{$table-padding-small}">
                            <fo:block>DEPOSITS</fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                </fo:table-header>

                <fo:table-footer>
                    <fo:table-row use-attribute-sets="table-footer-row">
                        <fo:table-cell number-columns-spanned="3" padding-left="{$table-padding-small}">
                            <fo:block>TOTAL WITHDRAWALS &amp; DEPOSITS</fo:block>
                        </fo:table-cell>
                        <fo:table-cell text-align="right">
                            <fo:block>
                                <xsl:value-of select="rp:sectionattributes/rp:sa[@name='total_withdrawals']"/>
                            </fo:block>
                        </fo:table-cell>
                        <fo:table-cell text-align="right" padding-right="{$table-padding-small}">
                            <fo:block>
                                <xsl:value-of select="rp:sectionattributes/rp:sa[@name='total_deposits']"/>
                            </fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row use-attribute-sets="table-footer-final-row">
                        <fo:table-cell number-columns-spanned="4" padding-left="{$table-padding-small}">
                            <fo:block>NET WITHDRAWALS &amp; DEPOSITS</fo:block>
                        </fo:table-cell>
                        <fo:table-cell text-align="right" padding-right="{$table-padding-small}">
                            <fo:block>
                                <xsl:value-of select="rp:sectionattributes/rp:sa[@name='net_activity']"/>
                            </fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                </fo:table-footer>

                <fo:table-body>
                    <xsl:for-each select="rp:r">
                        <fo:table-row use-attribute-sets="table-row">
                            <fo:marker marker-class-name="continued">
                                <xsl:if test="position() != 1">
                                    <fo:inline> (Continued)</fo:inline>
                                </xsl:if>
                            </fo:marker>
                            <fo:table-cell padding-left="{$table-padding-small}">
                                <fo:block><xsl:value-of select="rp:c[@ref='date']"/></fo:block>
                            </fo:table-cell>

                            <fo:table-cell>
                                <fo:block><xsl:value-of select="rp:c[@ref='transaction_type']"/></fo:block>
                            </fo:table-cell>

                            <fo:table-cell xsl:use-attribute-sets="description-cell">
                                <fo:block><xsl:value-of select="rp:c[@ref='description']"/></fo:block>
                            </fo:table-cell>

                            <fo:table-cell text-align="right">
                                <fo:block><xsl:value-of select="rp:c[@ref='withdrawals']"/></fo:block>
                            </fo:table-cell>

                            <fo:table-cell text-align="right" padding-right="{$table-padding-small}">
                                <fo:block><xsl:value-of select="rp:c[@ref='deposits']"/></fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                    </xsl:for-each>
                </fo:table-body>
            </fo:table>
        </fo:block>
    </xsl:template>

    <xsl:template match="rp:section[@name='OtherActivity']">
        <xsl:choose>
            <xsl:when test="count(rp:r) = 0">
                <fo:block>No other activity to display</fo:block>
            </xsl:when>
            <xsl:otherwise>
                <fo:block>
                    <fo:table xsl:use-attribute-sets="table-data">
                        <fo:table-column column-width="{$col-width-trade-date}"/>
                        <fo:table-column column-width="{$col-width-description-other-activity}"/>
                        <fo:table-column column-width="{$col-width-symbol}"/>
                        <fo:table-column column-width="{$col-width-transaction-type}"/>
                        <fo:table-column column-width="{$col-width-quantity}"/>
                        <fo:table-column column-width="{$col-width-price}"/>
                        <fo:table-column column-width="{$col-width-amount-col1}"/>
                        <fo:table-column column-width="{$col-width-amount-col2}"/>

                        <fo:table-header>
                            <fo:table-row use-attribute-sets="table-header-first-line">
                                <fo:table-cell number-columns-spanned="8" xsl:use-attribute-sets="table-header-first-line-cell">
                                    <fo:block>
                                        OTHER ACTIVITY
                                        <fo:retrieve-table-marker retrieve-class-name="continued"
                                                                  retrieve-position-within-table="first-starting"
                                                                  retrieve-boundary-within-table="table"/>
                                    </fo:block>
                                </fo:table-cell>
                            </fo:table-row>
                            <fo:table-row use-attribute-sets="table-header-second-line">
                                <fo:table-cell padding-left="{$table-padding-small}">
                                    <fo:block>DATE</fo:block>
                                </fo:table-cell>

                                <fo:table-cell>
                                    <fo:block>DESCRIPTION</fo:block>
                                </fo:table-cell>

                                <fo:table-cell>
                                    <fo:block>SYMBOL&#47;</fo:block>
                                    <fo:block>CUSIP</fo:block>
                                </fo:table-cell>

                                <fo:table-cell>
                                    <fo:block>TRANSACTION</fo:block>
                                    <fo:block>TYPE</fo:block>
                                </fo:table-cell>

                                <fo:table-cell text-align="right">
                                    <fo:block>QUANTITY</fo:block>
                                </fo:table-cell>

                                <fo:table-cell text-align="right">
                                    <fo:block>PRICE</fo:block>
                                </fo:table-cell>

                                <fo:table-cell text-align="right">
                                    <fo:block>AMOUNT</fo:block>
                                    <fo:block>DEBITED</fo:block>
                                </fo:table-cell>

                                <fo:table-cell text-align="right" padding-right="{$table-padding-small}">
                                    <fo:block>AMOUNT</fo:block>
                                    <fo:block>CREDITED</fo:block>
                                </fo:table-cell>
                            </fo:table-row>
                        </fo:table-header>

                        <fo:table-footer>
                            <fo:table-row use-attribute-sets="table-footer-row">
                                <fo:table-cell number-columns-spanned="6" padding-left="{$table-padding-small}">
                                    <fo:block>TOTAL OTHER ACTIVITY</fo:block>
                                </fo:table-cell>
                                <fo:table-cell text-align="right">
                                    <fo:block>
                                        <xsl:call-template name="dollar">
                                            <xsl:with-param name="raw-num" select="rp:sectionattributes/rp:sa[@name='total_other_activity_debit']"/>
                                        </xsl:call-template>
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell text-align="right" padding-right="{$table-padding-small}">
                                    <fo:block>
                                        <xsl:call-template name="dollar">
                                            <xsl:with-param name="raw-num" select="rp:sectionattributes/rp:sa[@name='total_other_activity_credit']"/>
                                        </xsl:call-template>
                                    </fo:block>
                                </fo:table-cell>
                            </fo:table-row>
                            <fo:table-row use-attribute-sets="table-footer-final-row">
                                <fo:table-cell number-columns-spanned="7" padding-left="{$table-padding-small}">
                                    <fo:block>NET OTHER ACTIVITY</fo:block>
                                </fo:table-cell>
                                <fo:table-cell text-align="right" padding-right="{$table-padding-small}">
                                    <fo:block>
                                        <xsl:call-template name="dollar">
                                            <xsl:with-param name="raw-num" select="rp:sectionattributes/rp:sa[@name='net_other_activity']"/>
                                        </xsl:call-template>
                                    </fo:block>
                                </fo:table-cell>
                            </fo:table-row>
                        </fo:table-footer>

                        <fo:table-body>
                            <xsl:for-each select="rp:r">
                                <fo:table-row use-attribute-sets="table-row">
                                    <fo:marker marker-class-name="continued">
                                        <xsl:if test="position() != 1">
                                            <fo:inline> (Continued)</fo:inline>
                                        </xsl:if>
                                    </fo:marker>

                                    <fo:table-cell padding-left="{$table-padding-small}">
                                        <fo:block><xsl:value-of select="rp:c[@ref='date']"/></fo:block>
                                    </fo:table-cell>

                                    <fo:table-cell xsl:use-attribute-sets="description-cell">
                                        <fo:block>
                                            <fo:block><xsl:value-of select="rp:c[@ref='description']"/></fo:block>
                                            <xsl:call-template name="separatedStringToBlocks">
                                                <xsl:with-param name="data" select="rp:c[@ref='memos_delim']"/>
                                            </xsl:call-template>
                                            <xsl:call-template name="separatedStringToBlocks">
                                                <xsl:with-param name="data" select="rp:c[@ref='trailers_delim']"/>
                                                <xsl:with-param name="isTrailerCode">true</xsl:with-param>
                                            </xsl:call-template>
                                        </fo:block>
                                    </fo:table-cell>

                                    <fo:table-cell>
                                        <fo:block><xsl:value-of select="rp:c[@ref='symbol']"/></fo:block>
                                    </fo:table-cell>

                                    <fo:table-cell>
                                        <fo:block><xsl:value-of select="rp:c[@ref='transaction_type']"/></fo:block>
                                    </fo:table-cell>

                                    <fo:table-cell text-align="right">
                                        <fo:block><xsl:value-of select="format-number(rp:c[@ref='quantity'], '#,##0')"/></fo:block>
                                    </fo:table-cell>

                                    <fo:table-cell text-align="right">
                                        <fo:block><xsl:value-of select="format-number(rp:c[@ref='price'], '#,##0.0000')"/></fo:block>
                                    </fo:table-cell>

                                    <fo:table-cell text-align="right">
                                        <fo:block><xsl:value-of select="format-number(rp:c[@ref='amount_debited'], '#,##0.00')"/></fo:block>
                                    </fo:table-cell>

                                    <fo:table-cell text-align="right" padding-right="{$table-padding-small}">
                                        <fo:block><xsl:value-of select="format-number(rp:c[@ref='amount_credited'], '#,##0.00')"/></fo:block>
                                    </fo:table-cell>
                                </fo:table-row>
                            </xsl:for-each>
                        </fo:table-body>
                    </fo:table>
                </fo:block>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="transaction-summary-table-row-open">
        <xsl:param name="row-name"/>
        <xsl:param name="currency-short"/>
        <xsl:param name="period-value"/>
        <xsl:param name="ytd-value"/>

        <xsl:variable name="num-format">#,##0.00</xsl:variable>

        <fo:table-row xsl:use-attribute-sets="transaction-summary-section-open">
            <fo:table-cell padding-left="{$table-padding-medium}">
                <fo:block><xsl:value-of select="$row-name"/></fo:block>
            </fo:table-cell>
            <fo:table-cell text-align="right">
                <fo:block><xsl:value-of select="$currency-short"/></fo:block>
            </fo:table-cell>
            <fo:table-cell text-align="right">
                <fo:block>
                    <xsl:value-of select="format-number($period-value, $num-format)"/>
                </fo:block>
            </fo:table-cell>
            <fo:table-cell text-align="right">
                <fo:block><xsl:value-of select="$currency-short"/></fo:block>
            </fo:table-cell>
            <fo:table-cell text-align="right" padding-right="{$table-padding-small}">
                <fo:block>
                    <xsl:value-of select="format-number($ytd-value, $num-format)"/>
                </fo:block>
            </fo:table-cell>
        </fo:table-row>
    </xsl:template>

    <xsl:template name="transaction-summary-table-row-close">
        <xsl:param name="row-name"/>
        <xsl:param name="currency-short"/>
        <xsl:param name="period-value"/>
        <xsl:param name="ytd-value"/>

        <xsl:variable name="num-format">#,##0.00</xsl:variable>

        <fo:table-row xsl:use-attribute-sets="transaction-summary-section-close">
            <fo:table-cell padding-left="{$table-padding-medium}">
                <fo:block><xsl:value-of select="$row-name"/></fo:block>
            </fo:table-cell>
            <fo:table-cell text-align="right">
                <fo:block><xsl:value-of select="$currency-short"/></fo:block>
            </fo:table-cell>
            <fo:table-cell text-align="right">
                <fo:block>
                    <xsl:value-of select="format-number($period-value, $num-format)"/>
                </fo:block>
            </fo:table-cell>
            <fo:table-cell text-align="right">
                <fo:block><xsl:value-of select="$currency-short"/></fo:block>
            </fo:table-cell>
            <fo:table-cell text-align="right" padding-right="{$table-padding-small}">
                <fo:block>
                    <xsl:value-of select="format-number($ytd-value, $num-format)"/>
                </fo:block>
            </fo:table-cell>
        </fo:table-row>
    </xsl:template>

    <xsl:template name="transaction-summary-table-row-single">
        <xsl:param name="row-name"/>
        <xsl:param name="currency-short"/>
        <xsl:param name="period-value"/>
        <xsl:param name="ytd-value"/>

        <xsl:variable name="num-format">#,##0.00</xsl:variable>

        <fo:table-row xsl:use-attribute-sets="transaction-summary-section-single">
            <fo:table-cell padding-left="{$table-padding-medium}">
                <fo:block><xsl:value-of select="$row-name"/></fo:block>
            </fo:table-cell>
            <fo:table-cell text-align="right">
                <fo:block><xsl:value-of select="$currency-short"/></fo:block>
            </fo:table-cell>
            <fo:table-cell text-align="right">
                <fo:block>
                    <xsl:value-of select="format-number($period-value, $num-format)"/>
                </fo:block>
            </fo:table-cell>
            <fo:table-cell text-align="right">
                <fo:block><xsl:value-of select="$currency-short"/></fo:block>
            </fo:table-cell>
            <fo:table-cell text-align="right" padding-right="{$table-padding-small}">
                <fo:block>
                    <xsl:value-of select="format-number($ytd-value, $num-format)"/>
                </fo:block>
            </fo:table-cell>
        </fo:table-row>
    </xsl:template>
    
    <xsl:template name="account-value-summary-table-row">
        <xsl:param name="title"/>
        <xsl:param name="currency-short"/>
        <xsl:param name="value-current"/>
        <xsl:param name="value-old"/>
        <xsl:param name="bold" select="false()"/>

        <xsl:variable name="num-format">$#,##0.00</xsl:variable>

        <xsl:choose>
            <xsl:when test="string-length($value-current) and string-length($value-old)">
                <fo:table-row xsl:use-attribute-sets="account-value-summary-table">

                    <xsl:if test="$bold = 'true'">
                        <xsl:attribute name="font-weight">bold</xsl:attribute>
                        <xsl:attribute name="border-bottom">2pt solid black</xsl:attribute>
                    </xsl:if>

                    <fo:table-cell padding-left="{$table-padding-large}">
                        <fo:block><xsl:value-of select="$title"/></fo:block>
                    </fo:table-cell>
                    <fo:table-cell text-align="right">
                        <fo:block><xsl:value-of select="$currency-short"/></fo:block>
                    </fo:table-cell>
                    <fo:table-cell text-align="right">
                        <fo:block>
                            <xsl:value-of select="format-number($value-current, $num-format)"/>
                        </fo:block>
                    </fo:table-cell>
                    <fo:table-cell text-align="right">
                        <fo:block><xsl:value-of select="$currency-short"/></fo:block>
                    </fo:table-cell>
                    <fo:table-cell text-align="right" padding-right="{$table-padding-medium}">
                        <fo:block>
                            <xsl:value-of select="format-number($value-old, $num-format)"/>
                        </fo:block>
                    </fo:table-cell>
                </fo:table-row>
            </xsl:when>
            <xsl:when test="string-length($value-current)">
                <!-- this clause can go after go-live -->
                <fo:table-row xsl:use-attribute-sets="account-value-summary-table">

                    <xsl:if test="$bold = 'true'">
                        <xsl:attribute name="font-weight">bold</xsl:attribute>
                        <xsl:attribute name="border-bottom">2pt solid black</xsl:attribute>
                    </xsl:if>

                    <fo:table-cell padding-left="{$table-padding-large}">
                        <fo:block><xsl:value-of select="$title"/></fo:block>
                    </fo:table-cell>
                    <fo:table-cell text-align="right">
                        <fo:block><xsl:value-of select="$currency-short"/></fo:block>
                    </fo:table-cell>
                    <fo:table-cell text-align="right">
                        <fo:block>
                            <xsl:value-of select="format-number($value-current, $num-format)"/>
                        </fo:block>
                    </fo:table-cell>
                    <fo:table-cell text-align="right">
                        <fo:block></fo:block>
                    </fo:table-cell>
                    <fo:table-cell text-align="right" padding-right="{$table-padding-medium}">
                        <fo:block></fo:block>
                    </fo:table-cell>
                </fo:table-row>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="LINE_DATA">
        <xsl:for-each select="LINE">
            <fo:block>
                <xsl:value-of select="."/>
            </fo:block>
        </xsl:for-each>
    </xsl:template>

    <xsl:template name="tokenize">
        <xsl:param name="text"/>
        <xsl:param name="delimiter"/>

        <xsl:variable name="newtext">
            <xsl:choose>
                <xsl:when test="contains($text, $delimiter)">
                    <xsl:value-of select="normalize-space($text)" />
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="concat(normalize-space($text), $delimiter)"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="first" select="substring-before($newtext, $delimiter)" />
        <xsl:variable name="remaining" select="substring-after($newtext, $delimiter)" />

        <token>
            <xsl:value-of select="$first" />
        </token>

        <xsl:if test="$remaining">
            <xsl:call-template name="tokenize">
                <xsl:with-param name="text" select="$remaining" />
                <xsl:with-param name="delimiter" select="$delimiter" />
            </xsl:call-template>
        </xsl:if>
    </xsl:template>

    <xsl:template name="separatedStringToBlocks">
        <xsl:param name="data"/>
        <xsl:param name="isTrailerCode" select="false"/>

        <xsl:if test="string-length($data)">
            <xsl:variable name="lines">
                <xsl:call-template name="tokenize">
                    <xsl:with-param name="text"><xsl:value-of select="$data"/></xsl:with-param>
                    <xsl:with-param name="delimiter">||||</xsl:with-param>
                </xsl:call-template>
            </xsl:variable>

            <xsl:for-each select="exsl:node-set($lines)//token">
                <xsl:if test="string-length(.)">
                    <fo:block>
                        <xsl:choose>
                            <xsl:when test="$isTrailerCode = 'true'">
                                <xsl:call-template name="trailer-text">
                                    <xsl:with-param name="trailer-code" select="."/>
                                </xsl:call-template>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="."/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </fo:block>
                </xsl:if>
            </xsl:for-each>
        </xsl:if>

    </xsl:template>

    <xsl:template name="dollar">
        <xsl:param name="raw-num"/>

        <!-- eat up the sign -->
        <xsl:variable name="num-format-fn">#,##0.00</xsl:variable>
        <xsl:variable name="sign">
            <xsl:if test="$raw-num &lt; 0">-</xsl:if>
        </xsl:variable>

        <!-- our xsl does not have abs... it's quite weak, nothing to look at -->
        <xsl:variable name="abs">
            <xsl:choose>
                <xsl:when test="$raw-num &lt; 0"><xsl:value-of select="-1 * $raw-num"/></xsl:when>
                <xsl:otherwise><xsl:value-of select="$raw-num"/></xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:variable name="num">
            <xsl:value-of select="format-number($abs, $num-format-fn)"/>
        </xsl:variable>

        <xsl:if test="string-length($raw-num) &gt; 0">
            <xsl:value-of select="concat($sign, '$', $num)"/>
        </xsl:if>
    </xsl:template>
</xsl:stylesheet>
