<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:fo="http://www.w3.org/1999/XSL/Format">

<xsl:output method="xml" version="1.0" indent="yes"/>

<xsl:attribute-set name="disclaimer-paragraph">
    <xsl:attribute name="margin-bottom">8pt</xsl:attribute>
</xsl:attribute-set>

<xsl:attribute-set name="disclaimer-block">
    <xsl:attribute name="font-weight">normal</xsl:attribute>
    <xsl:attribute name="font-size">8pt</xsl:attribute>
    <xsl:attribute name="text-align">left</xsl:attribute>
    <xsl:attribute name="margin-bottom">30pt</xsl:attribute>
</xsl:attribute-set>

<xsl:template name="glossary-item">
    <xsl:param name="abbr"/>
    <xsl:param name="text"/>

    <fo:inline space-end="8pt">
        <xsl:value-of select="$abbr"/>
        =
        <fo:inline font-weight="bold"><xsl:value-of select="$text"/></fo:inline>
    </fo:inline>

</xsl:template>

<xsl:template name="cs-report-disclaimer">
    <fo:block font-size="10pt" font-weight="bold" margin-bottom="10pt">DISCLAIMER</fo:block>
    <fo:block xsl:use-attribute-sets="disclaimer-block">
        <fo:block xsl:use-attribute-sets="disclaimer-paragraph">Credit Suisse Securities (Canada), Inc. (&#8220;CSSC&#8221;) is a subsidiary of Credit Suisse AG and a member of the Canadian Investor Protection Fund. Customer's accounts are protected by the Canadian Investor Protection Fund within specified limits. A brochure describing the nature and limits of coverage is available upon request.</fo:block>
        <fo:block xsl:use-attribute-sets="disclaimer-paragraph">Monthly statements will be conclusive and binding unless you notify CSSC of any objection within five (5) business days after receipt of such monthly statement if the objection could not have been raised at the time such monthly statement was received by you. All other documents will be conclusive and binding unless you notify CSSC of any objection immediately. You must confirm in writing within one business day any oral notice of objection you give to CSSC.</fo:block>
        <fo:block xsl:use-attribute-sets="disclaimer-paragraph">Any free credit balances represent funds payable on demand which, although properly recorded in CSSC's books, are not segregated and may be used in the conduct of our business.</fo:block>
        <fo:block xsl:use-attribute-sets="disclaimer-paragraph">All transactions are subject to applicable laws, rules, practices and customs, including the by-laws and regulations of the Investment Industry Regulatory Organization of Canada, and shall be subject to terms of any written agreement between you and CSSC.</fo:block>
        <fo:block xsl:use-attribute-sets="disclaimer-paragraph">Clients in British Columbia are entitled to certain additional information about CSSC, including information about commissions and fees, and about administrative proceedings that may relate to the firm and its staff.</fo:block>
        <fo:block xsl:use-attribute-sets="disclaimer-paragraph">A copy of CSSC's most recent financial statements, a listing of all directors and senior officers of CSSC, and the name of CSSCs representative and contra-broker, if applicable, are available upon request.</fo:block>
        <fo:block xsl:use-attribute-sets="disclaimer-paragraph">We suggest you retain this document for income tax purposes.</fo:block>
        <fo:block xsl:use-attribute-sets="disclaimer-paragraph">
            <xsl:call-template name="glossary-item">
                <xsl:with-param name="abbr">REST</xsl:with-param>
                <xsl:with-param name="text">Restricted</xsl:with-param>
            </xsl:call-template>
            <xsl:call-template name="glossary-item">
                <xsl:with-param name="abbr">NON VTG</xsl:with-param>
                <xsl:with-param name="text">Non Voting</xsl:with-param>
            </xsl:call-template>
            <xsl:call-template name="glossary-item">
                <xsl:with-param name="abbr">SUB</xsl:with-param>
                <xsl:with-param name="text">Subordinate</xsl:with-param>
            </xsl:call-template>
            <xsl:call-template name="glossary-item">
                <xsl:with-param name="abbr">VTG</xsl:with-param>
                <xsl:with-param name="text">Voting</xsl:with-param>
            </xsl:call-template>
            <xsl:call-template name="glossary-item">
                <xsl:with-param name="abbr">DSC</xsl:with-param>
                <xsl:with-param name="text">Deferred Sales Charge</xsl:with-param>
            </xsl:call-template>
        </fo:block>
    </fo:block>
    <fo:block xsl:use-attribute-sets="disclaimer-block">
        <fo:block xsl:use-attribute-sets="disclaimer-paragraph">Credit Suisse Securities (Canada) Inc. (ci-apr&#232;s &#171;CSSC&#187;) est une soci&#233;t&#233; affil&#233;e de Credit Suisse AG et member du Fonds canadien de protection des &#233;pargnants. Les comptes des clients sont prot&#233;g&#233;s par le Fonds canadien de protection des &#233;pargnants dans les limites d&#233;finies. Une brochure pr&#233;sentant la nature et les limites de la couverture est disponible sur demande.</fo:block>
        <fo:block xsl:use-attribute-sets="disclaimer-paragraph">Les relev&#233;s mensuels seront consid&#233;r&#233;s comme d&#233;finitifs et contraignants sauf objection de votre part &#224; CSSC au moment de la r&#233;ception de ces relev&#233;s mensuels ou dans les cinq (5) jours ouvrables suivant cette r&#233;ception. Tous les autres documents seront consid&#233;r&#233;s comme d&#233;finitifs et contraignants sauf objection imm&#233;diatement adress&#233;e &#224; CSSC. Toute objection adress&#233;e par oral &#224; CSSC devra &#234;tre confirm&#233;e par &#233;crit en lespace dun jour ouvrable.</fo:block>
        <fo:block xsl:use-attribute-sets="disclaimer-paragraph">Tout solde cr&#233;diteur libre repr&#233;sente des actifs payables sur demande qui, bien que correctement enregistr&#233;s dans les registres de CSSC, ne sont pas comptabilis&#233;s de mani&#232;re distincte et peuvent &#234;tre utilis&#233;s pour nos activit&#233;s.</fo:block>
        <fo:block xsl:use-attribute-sets="disclaimer-paragraph">Chaque transaction est soumise &#224; la l&#233;gislation, aux r&#232;gles, aux pratiques et aux usages applicables, y compris aux r&#232;glements et aux r&#233;glementations de l'Organisme canadien de r&#233;glementation du commerce des valeurs mobili&#232;res, et sera soumise aux conditions stipul&#233;es dans l'accord &#233;crit pass&#233; entre vous et CSSC.</fo:block>
        <fo:block xsl:use-attribute-sets="disclaimer-paragraph">Un exemplaire des &#233;tats financiers de CSSC les plus r&#233;cents et la liste de tous les directeurs et cadres sup&#233;rieurs de CSSC sont disponibles sur demande.</fo:block>
        <fo:block xsl:use-attribute-sets="disclaimer-paragraph">Nos clients en Colombie-Britannique peuvent obtenir certaines informations compl&#233;mentaires sur CSSC, notamment sur les commissions et les frais ainsi que sur les proc&#233;dures administratives relatives &#224; l'entreprise ou &#224; notre personnel.</fo:block>
        <fo:block xsl:use-attribute-sets="disclaimer-paragraph">Nous vous conseillons de conserver ce document pour votre d&#233;claration d'imp&#244;t sur le revenu.</fo:block>
        <fo:block xsl:use-attribute-sets="disclaimer-paragraph">
            <xsl:call-template name="glossary-item">
                <xsl:with-param name="abbr">REST</xsl:with-param>
                <xsl:with-param name="text">Droit De Votre Restreint</xsl:with-param>
            </xsl:call-template>
            <xsl:call-template name="glossary-item">
                <xsl:with-param name="abbr">NON VTG</xsl:with-param>
                <xsl:with-param name="text">Aucun Droit De Votre</xsl:with-param>
            </xsl:call-template>
            <xsl:call-template name="glossary-item">
                <xsl:with-param name="abbr">SUB</xsl:with-param>
                <xsl:with-param name="text">Droit De Votre Subalterne</xsl:with-param>
            </xsl:call-template>
            <xsl:call-template name="glossary-item">
                <xsl:with-param name="abbr">VTG</xsl:with-param>
                <xsl:with-param name="text">Droit De Vote</xsl:with-param>
            </xsl:call-template>
            <xsl:call-template name="glossary-item">
                <xsl:with-param name="abbr">DSC</xsl:with-param>
                <xsl:with-param name="text">Droits Dentr&#233;e Diff&#233;r&#233;s</xsl:with-param>
            </xsl:call-template>
        </fo:block>
    </fo:block>
</xsl:template>
</xsl:stylesheet>
